CXX=g++
CXXFLAGS=-Werror -Wall -Wextra -O3 -std=c++11 -pedantic
RULES=makefile.rules
LOGIN=digius_p
PROJECT=chess

# TARGETS
BINARY=chess
LIB=libFIXME.so
LIB_LISTENER=lib-listener.so
LIB_AI=libai.so

# SOURCES
AI_SRC= $(addprefix src/, \
  move.cc                 \
  player.cc               \
  chessboard.cc           \
  sexy-move.cc            \
  allowed-moves.cc        \
  chessboard-adapter.cc   \
  position.cc             \
  ai.cc                   \
  best-ai-ever.cc         \
  minimax.cc              \
  evaluator/feature-extractor.cc  \
  evaluator/feature.cc    \
  evaluator/fitness-evaluator.cc  \
  evaluator/score-feature.cc      \
  evaluator/matrix-feature.cc        \
  evaluator/threat-feature.cc        \
)

# OBJS
AI_OBJS=$(AI_SRC:.cc=.o)
LISTENER_OBJS=src/listener.o src/sexy-listener.o

-include $(RULES)

# fPIC
%.o: %.cc
	$(CXX) $(CXXFLAGS) -fPIC -c $^ -o $@

all: ai


ai: $(AI_OBJS)
	$(CXX) $(CXXFLAGS) -shared -fPIC -o $(LIB_AI) $(AI_OBJS)

$(BINARY): src/toto.o $(AI_OBJS)
	$(CXX) $(CXXFLAGS) -o $(BINARY) -ldl src/toto.o $(AI_OBJS)

$(LIB_LISTENER): $(LISTENER_OBJS)
	$(CXX) $(CXXFLAGS) -shared -fPIC -o $@ $^

clean:
	$(RM) -rf $(BINARY) $(LIB) $(AI_OBJS) src/toto.o $(LIB_AI)
	$(RM) -rf doc/html $(LIB_LISTENER) $(LISTENER_OBJS)

cleanlib:
	rm *.o *.so

distclean: clean
	$(RM) $(RULES)

check: $(BINARY) $(LIB_LISTENER)
	./tests/check.sh

doc: doc/Doxyfile
	doxygen doc/Doxyfile

.PHONY: all clean distclean check doc
