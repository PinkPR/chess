#include "ai-dummy.hh"
#include "move.hh"
#include "position.hh"
#include "chessboard.hh"

namespace dummy
{
  AiDummy::AiDummy(Color color)
    : Ai(color)
  {
    turn_ = 0;
  }

  AiDummy::~AiDummy()
  {
  }

  const std::string&
  AiDummy::name_get() const
  {
    static std::string name = "digius_p";
    return name;
  }

  Move
  AiDummy::move_get()
  {
    // file, rank
    // column, line
    Position start;
    Position stop;

    if (this->color_get() == BLACK)
    {
      if (turn_ == 0)
      {
        // move the knight b8 to c6
        start = Position(ST_FILE(2), ST_RANK(8));
        stop = Position(ST_FILE(3), ST_RANK(6));
      }
      else if (turn_ % 2 == 0)
      {
        // move rook b8 to a8
        start = Position(ST_FILE(2), ST_RANK(8));
        stop = Position(ST_FILE(1), ST_RANK(8));
      }
      else
      {
        // move tower a8 to b8
        start = Position(ST_FILE(1), ST_RANK(8));
        stop = Position(ST_FILE(2), ST_RANK(8));
      }
    }
    else
    {
      if (turn_ == 0)
      {
        // move the knight g1 to f3
        start = Position(ST_FILE(7), ST_RANK(1));
        stop = Position(ST_FILE(6), ST_RANK(3));
      }
      else if (turn_ % 2 == 0)
      {
        // move tower g1 to h1
        start = Position(ST_FILE(7), ST_RANK(1));
        stop = Position(ST_FILE(8), ST_RANK(1));
      }
      else
      {
        // move tower h1 to g1
        start = Position(ST_FILE(8), ST_RANK(1));
        stop = Position(ST_FILE(7), ST_RANK(1));
      }
    }

    this->turn_++;
    return Move(start, stop);
  }

  void
  AiDummy::last_opponent_move_set(const Move& last_move)
  {
    last_move_ = &last_move;
  }

} // namespace dummy

/// This macro allows us to use your AI.
/// So do not forget to call this macro.
AI_EXPORT(dummy::AiDummy)
