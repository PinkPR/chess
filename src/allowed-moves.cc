/**
** Pierre-Olivier DI GIUSEPPE
*/

#include "allowed-moves.hh"
#include "chessboard.hh"

bool allowed::allowed_move(Move move,
                           Color color,
                           Chessboard& chessboard)
{
  std::pair<PieceType, Color> p = chessboard[move.start_get()];

  if (chessboard[move.end_get()].first != NONE
      && chessboard[move.end_get()].second == color)
    return false;

  if (move.start_get() == move.end_get())
    return false;

  if (p.second == color)
  {
    if (p.first == PAWN)
      return pawn(color, move, chessboard);
    if (p.first == QUEEN)
      return queen(color, move, chessboard);
    if (p.first == KING)
      return king(color, move, chessboard);
    if (p.first == KNIGHT)
      return knight(color, move, chessboard);
    if (p.first == BISHOP)
      return bishop(color, move, chessboard);
    if (p.first == ROOK)
      return rook(color, move, chessboard);
  }

  return false;
}

int allowed::pos_file_diff(const Position& orig, const Position& dest)
{
  return orig.file_get() - dest.file_get();
}

int allowed::pos_rank_diff(const Position& orig, const Position& dest)
{
  return orig.rank_get() - dest.rank_get();
}

int abs(int a)
{
  return a < 0 ? -a : a;
}

bool allowed::pawn_path_free(const Position& orig,
                             const Position& dest,
                             Chessboard& chessboard)
{
  Position pos(CAST_FILE ((orig.file_get() + dest.file_get()) / 2),
               CAST_RANK ((orig.rank_get() + dest.rank_get()) / 2));

  return chessboard[pos].first == NONE;
}

bool allowed::pawn(Color& color, Move& move, Chessboard& chessboard)
{
  const Position orig = move.start_get();
  const Position dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  std::pair<const PieceType, const Color> dest_state = chessboard[dest];

  if (color == BLACK)
  {
    if (rank_diff < 0) // check if the direction is correct
    {
      if (file_diff == 0) // origin position
      {
        // check if dest is free
        if (abs(rank_diff) == 1 && chessboard[dest].first == NONE)
          return true;
        else if (abs(rank_diff) == 2) // when the pawn starts
          return (dest_state.first == NONE
                  && allowed::pawn_path_free(orig, dest, chessboard)
                  && orig.rank_get() == WHITE_START_RAMK + 1);
        else //move of more than 2
          return false;
      }
      // wants to capture
      else if (abs(file_diff) == abs(rank_diff) && abs(file_diff) == 1)
        return (dest_state.first != NONE
                && dest_state.second == color);
    }
    else // didnt move in the right direction
      return false;
  }
  else if (color == WHITE) // if BLACK
  {
    if (rank_diff > 0) // check if the direction is correct
    {
      if (file_diff == 0) // origin position
      {
        // check if dest is free
        if (abs(rank_diff) == 1 && chessboard[dest].first == NONE)
          return true;
        else if (abs(rank_diff) == 2) // when the pawn starts
          return (dest_state.first == NONE
                  && allowed::pawn_path_free(orig, dest, chessboard)
                  && orig.rank_get() == BLACK_START_RAMK - 1);
        else //move of more than 2
          return false;
      }
      // wants to capture
      else if (abs(file_diff) == abs(rank_diff) && abs(file_diff) == 1)
        return (dest_state.first != NONE
                && dest_state.second != color);
    }
    else // didnt move in the right direction
      return false;
  }

  return false;
}

bool allowed::bishop(Color& color, Move& move, Chessboard& chessboard)
{
  const Position orig = move.start_get();
  const Position dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].first != NONE
      && chessboard[dest].second == color)
    return false;

  if (abs(file_diff) != abs(rank_diff)) //check if move is diagonal
    return false;

  int file = orig.file_get();
  int rank = orig.rank_get();

  Position pos(CAST_FILE file, CAST_RANK rank);

  while (pos != dest)
  {
    if (chessboard.get_piece(file, rank) && pos != orig)
      return false;

    if (file_diff < 0)
      file++;
    else
      file--;

    if (rank_diff < 0)
      rank++;
    else
      rank--;

    pos = Position(CAST_FILE file,
                   CAST_RANK rank);
  }

  return true;
}

bool allowed::knight(Color& color, Move& move, Chessboard& chessboard)
{
  const Position orig = move.start_get();
  const Position dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].first != NONE
      && chessboard[dest].second == color)
    return false;

  return ((abs(file_diff) == 1 && abs(rank_diff) == 2)
          || (abs(file_diff) == 2 && abs(rank_diff) == 1));
}

bool allowed::rook(Color& color, Move& move, Chessboard& chessboard)
{
  const Position orig = move.start_get();
  const Position dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].first != NONE
      && chessboard[dest].second == color)
    return false;

  if (file_diff != 0 && rank_diff != 0)
    return false;

  int file = orig.file_get();
  int rank = orig.rank_get();

  Position pos(CAST_FILE file, CAST_RANK rank);

  while (pos != dest)
  {
    if (chessboard[pos].first != NONE
        && pos != orig)
      return false;

    if (file_diff)
    {
      if (file_diff > 0)
        file--;
      else
        file++;
    }
    else
    {
      if (rank_diff > 0)
        rank--;
      else
        rank++;
    }

    pos = Position(CAST_FILE file,
                   CAST_RANK rank);
  }

  return true;
}

bool allowed::queen(Color& color, Move& move, Chessboard& chessboard)
{
  return (allowed::rook(color, move, chessboard)
          || allowed::bishop(color, move, chessboard));
}

bool allowed::king(Color& color, Move& move, Chessboard& chessboard)
{
  const Position orig = move.start_get();
  const Position dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (abs(file_diff) >= 2)
  {
    if ((rank_diff == 0 && abs(file_diff) == 2)
        && (allowed::big_castling(color,chessboard)
        || allowed::lil_castling(color, chessboard)))
      return true;
    else
      return false;
  }

  if (abs(file_diff) > 1 || abs(rank_diff) > 1)
    return false;

  if (chessboard[dest].first != NONE
      && chessboard[dest].second == color)
    return false;

  return true;
}

//Couleur du roi en echec
bool check(Color color,
           Chessboard& chessboard)
{
  int lol = 0;
  Position kingpos;
  Position dest;
  Move move;

  //find King
  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      if (chessboard.get_piece(i, j)
          && chessboard.get_piece(i, j)->second == color
          && chessboard.get_piece(i, j)->first == KING)
      {
        kingpos = Position(ST_FILE(i), ST_RANK(j));
        lol = 1;
      }
    }
  }

  if (!lol)
    return false;

  color = color == BLACK ? WHITE : BLACK;

  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      if (chessboard.get_piece(i, j)
          && chessboard.get_piece(i, j)->second == color)
      {
        dest = Position(ST_FILE(i), ST_RANK(j));
        move = Move(dest, kingpos);

        if (allowed::allowed_move(move, color, chessboard))
          return true;
      }
    }
  }

  return false;
}

bool allowed::big_castling(Color& color,
                           Chessboard& chessboard)
{
  PAIR* p1;
  PAIR* p2;

  if (color == WHITE)
  {
    p1 = chessboard.get_piece(5, 1);
    p2 = chessboard.get_piece(1, 1);

    if (p1 && p1->first == KING && p1->second == color
        && p2 && p2->first == ROOK && p2->second == color)
    {
      for (int i = 2; i <= 4; i++)
      {
        if (chessboard.get_piece(i, 1))
          return false;
      }
      return true;
    }
  }
  else
  {
    p1 = chessboard.get_piece(5, 8);
    p2 = chessboard.get_piece(1, 8);

    if (p1 && p1->first == KING && p1->second == color
        && p2 && p2->first == ROOK && p2->second == color)
    {
      for (int i = 2; i <= 4; i++)
      {
        if (chessboard.get_piece(i, 8))
          return false;
      }
      return true;
    }
  }

  return false;
}

bool allowed::lil_castling(Color& color,
                           Chessboard& chessboard)
{
  PAIR* p1;
  PAIR* p2;

  if (color == WHITE)
  {
    p1 = chessboard.get_piece(5, 1);
    p2 = chessboard.get_piece(8, 1);

    if (p1 && p1->first == KING && p1->second == color
        && p2 && p2->first == ROOK && p2->second == color)
    {
      for (int i = 6; i <= 7; i++)
      {
        if (chessboard.get_piece(i, 1))
          return false;
      }
      return true;
    }
  }
  else
  {
    p1 = chessboard.get_piece(5, 8);
    p2 = chessboard.get_piece(8, 8);

    if (p1 && p1->first == KING && p1->second == color
        && p2 && p2->first == ROOK && p2->second == color)
    {
      for (int i = 6; i <= 7; i++)
      {
        if (chessboard.get_piece(i, 8))
          return false;
      }
      return true;
    }
  }

  return false;
}
