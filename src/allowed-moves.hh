/**
** Pierre-Olivier DI GIUSEPPE
*/

/**
** boolean methods to accept a movement
*/

#ifndef ALLOWED_MOVES_HH
# define ALLOWED_MOVES_HH

# define CAST_FILE  (Position::File)
# define CAST_RANK  (Position::Rank)

# include "chessboard.hh"
# include "move.hh"
# include "position.hh"

class Chessboard;

namespace allowed
{
  bool allowed_move(Move move,
                    Color color,
                    Chessboard& chessboard);

  /*!
  ** Return the diff on file axe between 2 positions
  ** @param orig  origin position
  ** @param dest  destination position
  */
  int pos_file_diff(const Position& orig, const Position& dest);

  /*!
  ** Return the diff on rank axe between 2 positions
  ** @param orig  origin position
  ** @param dest  destination position
  */
  int pos_rank_diff(const Position& orig, const Position& dest);

  /*!
  ** Return true if the pawn can move on his FIRST move (2 squares)
  ** @param orig  origin position
  ** @param dest  destination position
  ** @chessboard  chessboard to be considered
  */
  bool pawn_path_free(const Position& orig,
                      const Position& dest,
                      Chessboard& chessboard);

  /*!
  ** Return true if the pawn is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool pawn(Color& color, Move& move, Chessboard& chessboard);

  /*!
  ** Return true if the bishop is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool bishop(Color& color, Move& move, Chessboard& chessboard);

  /*!
  ** Return true if the knight is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool knight(Color& color, Move& move, Chessboard& chessboard);

  /*!
  ** Return true if the rook is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool rook(Color& color, Move& move, Chessboard& chessboard);

  /*!
  ** Return true if the queen is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool queen(Color& color, Move& move, Chessboard& chessboard);

  /*!
  ** Return true if the queen is allowed to do this move
  **
  ** @param color   Color of the player moving
  ** @param move    The move to test
  ** @chessboard    The chessboard to be considered
  */
  bool king(Color& color, Move& move, Chessboard& chessboard);

  bool big_castling(Color& color,
                    Chessboard& chessboard);

  bool lil_castling(Color& color,
                    Chessboard& chessboard);
}

bool check(Color color, Chessboard& chessboard);

#endif /* ALLOWED_MOVES_HH */
