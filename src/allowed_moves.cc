/**
** Pierre-Olivier DI GIUSEPPE
*/

#include "allowed_moves.hh"

bool allowed_move(PieceType& Piece,
                  Move& move,
                  ChessboardAdapter& chessboard)
{
  return false;
}

int allowed::pos_file_diff(const Position& orig, const Position& dest)
{
  return orig.file_get() - dest.file_get();
}

int allowed::pos_rank_diff(const Position& orig, const Position& dest)
{
  return orig.rank_get() - dest.rank_get();
}

int abs(int a)
{
  return a < 0 ? -a : a;
}

bool allowed::pawn_path_free(const Position& orig,
                             const Position& dest,
                             ChessboardAdapter& chessboard)
{
  Position pos(CAST_FILE ((orig.file_get() + dest.file_get()) / 2),
               CAST_RANK ((orig.rank_get() + dest.rank_get()) / 2));

  return chessboard[pos].first == NONE;
}

bool allowed::pawn(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  const Position& orig = move.start_get();
  const Position& dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  std::pair<const PieceType, const Color> dest_state = chessboard[dest];

  if (color == WHITE)
  {
    if (rank_diff < 0) // check if the direction is correct
    {
      if (file_diff == 0) // origin position
      {
        // check if dest is free
        if (abs(rank_diff) == 1 && chessboard[dest].first == NONE)
          return true;
        else if (abs(rank_diff) == 2) // when the pawn starts
          return (dest_state.first == NONE
                  && allowed::pawn_path_free(orig, dest, chessboard)
                  && orig.rank_get() == WHITE_START_RAMK + 1);
        else //move of more than 2
          return false;
      }
      // wants to capture
      else if (abs(file_diff) == abs(rank_diff) && abs(file_diff) == 1)
        return dest_state.second != color;
    }
    else // didnt move in the right direction
      return false;
  }
  else if (color == BLACK) // if BLACK
  {
    if (rank_diff > 0) // check if the direction is correct
    {
      if (file_diff == 0) // origin position
      {
        // check if dest is free
        if (abs(rank_diff) == 1 && chessboard[dest].first == NONE)
          return true;
        else if (abs(rank_diff) == 2) // when the pawn starts
          return (dest_state.first == NONE
                  && allowed::pawn_path_free(orig, dest, chessboard)
                  && orig.rank_get() == BLACK_START_RAMK - 1);
        else //move of more than 2
          return false;
      }
      // wants to capture
      else if (abs(file_diff) == abs(rank_diff) && abs(file_diff) == 1)
        return dest_state.second != color;
    }
    else // didnt move in the right direction
      return false;
  }

  return false;
}

bool allowed::bishop(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  const Position orig = move.start_get();
  const Position& dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].second == color)
    return false;

  if (abs(file_diff) != abs(rank_diff)) //check if move is diagonal
    return false;

  int file = orig.file_get();
  int rank = orig.rank_get();

  Position pos(CAST_FILE file, CAST_RANK rank);

  while (pos != dest)
  {
    if (chessboard[pos].first != NONE)
      return false;

    if (file_diff < 0)
      file--;
    else
      file++;

    if (rank_diff < 0)
      rank--;
    else
      rank++;

    pos = Position(CAST_FILE file,
                   CAST_RANK rank);
  }

  return true;
}

bool allowed::knight(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  const Position orig = move.start_get();
  const Position& dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].second == color)
    return false;

  return ((abs(file_diff) == 2 && abs(rank_diff) == 3)
          || (abs(file_diff) == 3 && abs(rank_diff) == 2));
}

bool allowed::rook(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  const Position orig = move.start_get();
  const Position& dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (chessboard[dest].second == color)
    return false;

  if (file_diff != 0 && rank_diff != 0)
    return false;

  int file = orig.file_get();
  int rank = orig.rank_get();

  Position pos(CAST_FILE file, CAST_RANK rank);

  while (pos != dest)
  {
    if (chessboard[pos].first != NONE)
      return false;

    if (file_diff)
      file++;
    else
      rank++;

    pos = Position(CAST_FILE file,
                   CAST_RANK rank);
  }

  return true;
}

bool allowed::queen(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  return (allowed::rook(color, move, chessboard)
          || allowed::bishop(color, move, chessboard));
}

bool allowed::king(Color& color, Move& move, ChessboardAdapter& chessboard)
{
  const Position orig = move.start_get();
  const Position& dest = move.end_get();

  int file_diff = allowed::pos_file_diff(orig, dest);
  int rank_diff = allowed::pos_rank_diff(orig, dest);

  if (abs(file_diff) > 1 || abs(rank_diff) > 1)
    return false;

  if (chessboard[dest].second == color)
    return false;

  return true;
}
