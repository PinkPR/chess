#include "best-ai-ever.hh"
#include "chessboard-printer.hh"

BestAiEver::BestAiEver(Color color)
           : Ai(color)
{
  this->name_ = "digius_p";
}

BestAiEver::~BestAiEver()
{
}

void BestAiEver::last_opponent_move_set(const Move& last_move)
{
  this->last_opponent_move_ = last_move;
  this->board_.make_move(last_move,
                         this->color_ == WHITE ? BLACK : WHITE);
}

Move BestAiEver::move_get()
{
  if (check(this->color_, this->board_))
  {
    std::list<Move> moves =
      this->board_.get_allowed_moves(this->color_);

    for (Move m : moves)
    {
      this->board_.make_move(m, this->color_);

      if (!check(this->color_, this->board_))
      {
        return m;
      }

      this->board_.revert_move();
    }
  }

  Move move = this->minimax_(this->board_, this->color_);
  this->board_.make_move(move, this->color_);

  return move;
}

const std::string& BestAiEver::name_get() const
{
  return this->name_;
}

AI_EXPORT(BestAiEver)
