#ifndef BEST_AI_EVER_HH
# define BEST_AI_EVER_HH

# include "ai.hh"
# include "minimax.hh"
# include "chessboard.hh"

class BestAiEver : public Ai
{
  public:
    BestAiEver(Color color);
    ~BestAiEver();

    void
    last_opponent_move_set(const Move& last_opponent_move) override;
    Move move_get() override;
    const std::string& name_get() const override;

  private:
    Chessboard board_;
    MiniMax minimax_;
    std::string name_;
};

#endif /* BEST_AI_EVER_HH */
