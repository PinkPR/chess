/**
** Pierre-Olivier DI GIUSEPPE
*/

#include "bitchessboard.hh"

BitChessboard::BitChessboard()
{
  this->board_ = static_cast<char*>(malloc(8 * sizeof (char*)));
}

BitChessboard::~BitChessboard()
{
  free(this->board_);
}

void BitChessboard::add_position(int x, int y)
{
  unsigned char tmp = 1 << (8 - x);

  this->board_[y - 1] |= tmp;
}

void BitChessboard::add_position(Position& position)
{
  int x = position.file_get();
  int y = position.rank_get();

  this->add_position(x, y);
}

void BitChessboard::rm_position(int x, int y)
{
  this->board_[y - 1] &= ((unsigned char) 255) ^ (1 << (8 - x));
}

void BitChessboard::rm_position(Position& position)
{
  int x = position.file_get();
  int y = position.rank_get();

  this->rm_position(x, y);
}

bool BitChessboard::allowed_position(int x, int y)
{
  return this->board_[y - 1] & (1 << (8 - x));
}

bool BitChessboard::allowed_position(Position& position)
{
  int x = position.file_get();
  int y = position.rank_get();

  return this->allowed_position(x, y);
}
