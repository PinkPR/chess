/**
** Pierre-Olivier DI GIUSEPPE
*/

#ifndef BITCHESSBOARD_HH
# define BITCHESSBOARD_HH

# include <cstdlib>
# include "chessboard-adapter.hh"
# include "position.hh"

class BitChessboard : public ChessboardAdapter
{
  public:
    BitChessboard();
    ~BitChessboard();

  public:
    void add_position(int x, int y);
    void add_position(Position& position);
    void rm_position(int x, int y);
    void rm_position(Position& position);
    bool allowed_position(int x, int y);
    bool allowed_position(Position& position);

  private:
    char* board_;
};

#endif /* BITCHESSBOARD_HH */
