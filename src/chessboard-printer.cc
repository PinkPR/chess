#include "chessboard-printer.hh"

#define GREEN   "\033[32m"
#define RED     "\033[31m"
#define BLANK   "\033[0m"

void chessboard_printer::print(Chessboard& chessboard)
{
  //system("clear");

  std::pair<PieceType, Color> p;

  for (int i = 0; i <= 8; i++)
  {
    if (i)
      std::cout << i << " ";
    else
      std::cout << "  ";

    for (int j = 1; j <= 8; j++)
    {
      if (!i)
      {
        if (j == 1)
          std::cout << "A ";
        if (j == 2)
          std::cout << "B ";
        if (j == 3)
          std::cout << "C ";
        if (j == 4)
          std::cout << "D ";
        if (j == 5)
          std::cout << "E ";
        if (j == 6)
          std::cout << "F ";
        if (j == 7)
          std::cout << "G ";
        if (j == 8)
          std::cout << "H ";

        continue;
      }

      p = chessboard[Position(static_cast<Position::File>(j),
                              static_cast<Position::Rank>(i))];

      if (p.first != NONE)
      {
        if (p.second == WHITE)
          std::cout << GREEN;
        else
          std::cout << RED;

        std::cout << get_char_piece(p.first);
        std::cout << " ";
      }
      else
        std::cout << "  ";

      std::cout << BLANK;
    }

    std::cout << std::endl;
  }

  std::cout << std::endl;
}

char chessboard_printer::get_char_piece(PieceType piece)
{
  if (piece == KING)
    return 'K';
  if (piece == QUEEN)
    return 'Q';
  if (piece == ROOK)
    return 'R';
  if (piece == BISHOP)
    return 'B';
  if (piece == KNIGHT)
    return 'N';
  else
    return 'P';
}
