#ifndef CHESSBOARD_PRINTER_HH
# define CHESSBOARD_PRINTER_HH

# include <iostream>
# include <cstdlib>

# include "chessboard.hh"

namespace chessboard_printer
{
  void print(Chessboard& chessboard);
  char get_char_piece(PieceType piece);
}

#endif /* CHESSBOARD_PRINTER_HH */
