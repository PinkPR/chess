/**
** Pierre-Olivier DI GIUSEPPE
*/

/**
** NOTES
**
** Exceptions have to be handled for position getter and setter
*/

#include "chessboard.hh"

Chessboard::Chessboard()
    : white_positions_(), black_positions_()
{
  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 8; j++)
    {
      this->chessboard_[i][j] = nullptr;
    }
  }

  this->set_board();
}


std::pair<const PieceType, const Color>
Chessboard::operator[](const Position& position) const
{
  std::pair<PieceType, Color>* p =
    this->chessboard_[position.file_get() - 1]
                     [position.rank_get() - 1];

  std::pair<PieceType, Color> ret;

  if (p)
  {
    ret = std::pair<PieceType, Color>(p->first, p->second);
  }
  else
  {
    ret = std::pair<PieceType, Color>(PieceType(6), Color(1));
  }

  return ret;
}

void
Chessboard::set_board()
{
  // PAWN init
  for (int i = 1; i <= 8; i++)
  {
    this->set_piece(new PAIR(PAWN, WHITE), i, 2);
    this->set_piece(new PAIR(PAWN, BLACK), i, 7);
  }

  // ROOKS
  this->set_piece(new PAIR(ROOK, WHITE), 8, 1);
  this->set_piece(new PAIR(ROOK, WHITE), 1, 1);
  this->set_piece(new PAIR(ROOK, BLACK), 1, 8);
  this->set_piece(new PAIR(ROOK, BLACK), 8, 8);

  //KNIGHTS
  this->set_piece(new PAIR(KNIGHT, WHITE), 2, 1);
  this->set_piece(new PAIR(KNIGHT, WHITE), 7, 1);
  this->set_piece(new PAIR(KNIGHT, BLACK), 2, 8);
  this->set_piece(new PAIR(KNIGHT, BLACK), 7, 8);

  //BISHOPS
  this->set_piece(new PAIR(BISHOP, WHITE), 3, 1);
  this->set_piece(new PAIR(BISHOP, WHITE), 6, 1);
  this->set_piece(new PAIR(BISHOP, BLACK), 3, 8);
  this->set_piece(new PAIR(BISHOP, BLACK), 6, 8);

  //QUEENS
  this->set_piece(new PAIR(QUEEN, WHITE), 4, 1);
  this->set_piece(new PAIR(QUEEN, BLACK), 4, 8);

  //KINGS
  this->set_piece(new PAIR(KING, WHITE), 5, 1);
  this->set_piece(new PAIR(KING, BLACK), 5, 8);
}

std::pair<PieceType, Color>* Chessboard::get_piece(int x, int y)
{
  return this->chessboard_[x - 1][y - 1];
}

void Chessboard::set_piece(PAIR* piece, int x, int y)
{
  Position p = Position(ST_FILE(x), ST_RANK(y));

  this->chessboard_[x - 1][y - 1] = piece;
}

const std::list<Position>& Chessboard::get_positions(Color color) const
{
    if (color == WHITE)
        return white_positions_;
    else
        return black_positions_;
}

void Chessboard::add_piece(PieceType,
                           Color,
                           Position&)
{
  /*std::pair<PieceType, Color>* elt =
    new std::pair<PieceType, Color>(piece, color);

  this->chessboard_[pos.file_get() - 1][pos.rank_get() - 1] = elt;*/
}

void Chessboard::add_piece(PieceType,
                           Color,
                           int,
                           int)
{
  /*Position pos(static_cast<Position::File>(x),
               static_cast<Position::Rank>(y));

  this->add_piece(piece, color, pos);*/
}

bool no_king(Chessboard& chessboard, Color color)
{
  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      if (chessboard.get_piece(i, j)
          && chessboard.get_piece(i, j)->first == KING
          && chessboard.get_piece(i, j)->second == color)
        return true;
    }
  }
  return false;
}

void Chessboard::make_move(Move move, Color color)
{
  Position orig = move.start_get();
  Position dest = move.end_get();

  int i = this->is_castling(color);
  if (i)
    return;

  std::pair<PieceType, Color>* piece =
    this->get_piece(orig.file_get(), orig.rank_get());
  std::pair<PieceType, Color>* eaten =
    this->get_piece(dest.file_get(), dest.rank_get());

  //this->set_ever_moved(orig.file_get(), orig.rank_get());

  int sm_eaten = NONE;

  if (eaten)
    sm_eaten = eaten->first;

  this->set_piece(nullptr, orig.file_get(), orig.rank_get());

  if (eaten)
    this->piece_cemetery_.push(eaten);

  this->set_piece(piece, dest.file_get(), dest.rank_get());

  SexyMove* sexmove = new SexyMove(move, sm_eaten, 0);

  this->moves_.push(sexmove);
}

std::list<Move> Chessboard::get_allowed_moves(Color& color)
{
  std::list<Move> move_list;
  Position orig;
  Position dest;
  Move move;

  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {

      if (this->get_piece(i, j)
          && this->get_piece(i, j)->second == color)
      {

        orig = Position(ST_FILE(i), ST_RANK(j));
        for (int x = 1; x <= 8; x++)
        {
          for (int y = 1; y <= 8; y++)
          {
            dest = Position(ST_FILE(x), ST_RANK(y));
            move = Move(orig, dest);

            if (allowed::allowed_move(move, color, *this))
            {
              this->make_move(move, color);
              if (!check(color, *this))
                  move_list.push_front(move);
              this->revert_move();
            }
          }
        }
      }
    }
  }

  return move_list;
}

bool Chessboard::revert_castling(Move move)
{
  Position orig = move.start_get();
  Position dest = move.end_get();

  if (((orig.file_get() - dest.file_get() == 2)
      || (orig.file_get() - dest.file_get() == -2))
      && this->get_piece(dest.file_get(), dest.rank_get())->first
          == KING)
  {
    PAIR* p = this->get_piece(dest.file_get(), dest.rank_get());

    if (p->second == WHITE)
    {
      if (dest.file_get() == 3)
      {
        this->set_piece(p, 5, 1);
        this->set_piece(this->get_piece(4, 1), 1, 1);
        this->set_piece(nullptr, 4, 1);
        this->set_piece(nullptr, 3, 1);
      }
      else
      {
        this->set_piece(p, 5, 1);
        this->set_piece(this->get_piece(6, 1), 8, 1);
        this->set_piece(nullptr, 7, 1);
        this->set_piece(nullptr, 6, 1);
      }
    }
    else
    {
      if (dest.file_get() == 3)
      {
        this->set_piece(p, 5, 8);
        this->set_piece(this->get_piece(4, 8), 1, 8);
        this->set_piece(nullptr, 4, 8);
        this->set_piece(nullptr, 3, 8);
      }
      else
      {
        this->set_piece(p, 5, 8);
        this->set_piece(this->get_piece(6, 8), 8, 8);
        this->set_piece(nullptr, 7, 8);
        this->set_piece(nullptr, 6, 8);
      }
    }
    return true;
  }

  return false;
}

void Chessboard::revert_move()
{
  std::pair<PieceType, Color>* piece = nullptr;
  std::pair<PieceType, Color>* eaten_piece = nullptr;

  SexyMove* last_move = this->moves_.top();
  this->moves_.pop();
  int eaten = last_move->get_eaten();

  Position orig = last_move->get_move().start_get();
  Position dest = last_move->get_move().end_get();

  if (this->revert_castling(last_move->get_move()))
    return;

  piece =
    this->get_piece(dest.file_get(), dest.rank_get());

  this->set_piece(piece, orig.file_get(), orig.rank_get());

  if (eaten != NONE)
  {
    eaten_piece = this->piece_cemetery_.top();
    this->piece_cemetery_.pop();

    this->set_piece(eaten_piece, dest.file_get(), dest.rank_get());
  }
  else
  {
    this->set_piece(nullptr, dest.file_get(), dest.rank_get());
  }
}

int Chessboard::ever_moved(int x, int y)
{
  return this->ever_moved_ & (1 << (x * y - 1));
}

void Chessboard::set_ever_moved(int x, int y)
{
  this->ever_moved_ |= 1 << (x * y - 1);
}

int Chessboard::is_castling(Color color)
{
  PAIR* p = nullptr;

  if (allowed::lil_castling(color, *this))
  {
    if (color == BLACK)
    {
      p = this->get_piece(5, 1);
      this->set_piece(nullptr, 5, 1);
      this->set_piece(this->get_piece(8, 1), 6, 1);
      this->set_piece(nullptr, 8, 1);
      this->set_piece(p, 7, 1);
      this->set_ever_moved(5, 1);
      this->set_ever_moved(8, 1);
    }
    else
    {
      p = this->get_piece(5, 8);
      this->set_piece(nullptr, 5, 8);
      this->set_piece(this->get_piece(8, 8), 6, 8);
      this->set_piece(nullptr, 8, 8);
      this->set_piece(p, 7, 8);
      this->set_ever_moved(5, 8);
      this->set_ever_moved(8, 8);
    }

    return 1;
  }
  else if (allowed::big_castling(color, *this))
  {
    if (color == BLACK)
    {
      p = this->get_piece(5, 1);
      this->set_piece(nullptr, 5, 1);
      this->set_piece(this->get_piece(1, 1), 4, 1);
      this->set_piece(p, 3, 1);
      this->set_ever_moved(5, 1);
      this->set_ever_moved(1, 1);
    }
    else
    {
      p = this->get_piece(4, 7);
      this->set_piece(nullptr, 5, 8);
      this->set_piece(this->get_piece(1, 8), 1, 8);
      this->set_piece(p, 3, 8);
      this->set_ever_moved(5, 8);
      this->set_ever_moved(1, 8);
    }

    return 1;
  }

  return 0;
}

int Chessboard::is_check(Color color)
{
  Position king_pos;
  int k = 0;

  for (int i = 1; i <= 8 && !k; i++)
  {
    for (int j = 1; j <= 8 && !k; j++)
    {
      if (this->get_piece(i, j)->first == KING
          && this->get_piece(i, j)->second == color)
      {
        king_pos = Position(static_cast<Position::File>(i),
                            static_cast<Position::Rank>(j));
        k = 1;
      }
    }
  }

  return this->is_threatened(king_pos, color);
}

int Chessboard::is_threatened(Position pos, Color color)
{
  Position current_pos;
  Move move;

  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      if (this->get_piece(i, j)->second != color)
      {
        current_pos = Position(static_cast<Position::File>(i),
                               static_cast<Position::Rank>(j));

        move = Move(current_pos, pos);

        if (allowed::allowed_move(move,
                                  this->get_piece(i, j)->second,
                                  *this))
        {
          return 1;
        }
      }
    }
  }

  return 0;
}

int Chessboard::is_mat(Color color)
{
  Position king_pos;
  Position current_pos;
  Move move;
  int k = 0;

  int a = 0;
  int b = 0;
  int x = 0;
  int y = 0;

  if (this->is_check(color))
  {
    for (int i = 1; i <= 8 && !k; i++)
    {
      for (int j = 1; j <= 8 && !k; j++)
      {
        if (this->get_piece(i, j)->first == KING
            && this->get_piece(i, j)->second == color)
        {
          king_pos = Position(static_cast<Position::File>(i),
                              static_cast<Position::Rank>(j));
          k = 1;
        }
      }
    }

    for (int i = -1; i <= 1; i++)
    {
      for (int j = -1; j <= 1; j++)
      {
        if (king_pos.file_get() + i >= 1
            && king_pos.file_get() + i <= 8
            && king_pos.rank_get() + j >= 1
            && king_pos.rank_get() + j <= 8)
        {
          x = king_pos.file_get() + i;
          j = king_pos.file_get() + j;
          current_pos = Position(static_cast<Position::File>(x),
                                 static_cast<Position::Rank>(y));

          if (!(this->chessboard_[x - 1][y - 1]))
          {
            b++;
            a += this->is_threatened(current_pos, color);
          }
        }
      }
    }

    if (a == b)
      return 1;
  }

  return 0;
}
