/**
** Pierre-Olivier DI GIUSEPPE
*/

#ifndef CHESSBOARD_HH
# define CHESSBOARD_HH

# define DIM                8
# define WHITE_START_RAMK   1
# define BLACK_START_RAMK   8
# define PAIR               std::pair<PieceType, Color>
# define ST_FILE            static_cast<Position::File>
# define ST_RANK            static_cast<Position::Rank>

# include <stack>
# include <iostream>
# include <list>

# include "chessboard-adapter.hh"
# include "position.hh"
# include "sexy-move.hh"
# include "allowed-moves.hh"
# include "player.hh"

class Chessboard : public ChessboardAdapter
{
  public:
    Chessboard();

    std::pair<const PieceType, const Color>
    operator[](const Position& position) const;

    void set_board();
    PAIR* get_piece(int x, int y);
    void set_piece(PAIR* piece, int x, int y);

    /*!
    ** Piece adder
    ** @param piece   piece to be added
    ** @param color   color of the piece to be added
    ** @param pos     where the piece has to be added
    */
    void add_piece(PieceType piece, Color color, Position& pos);
    void add_piece(PieceType piece, Color color, int x, int y);

    void make_move(Move move, Color color);

    std::list<Move> get_allowed_moves(Color& color);

    bool revert_castling(Move move);
    void revert_move();
    void reset();

    const std::list<Position>& get_positions(Color color) const;

    int ever_moved(int x, int y);
    void set_ever_moved(int x, int y);

    int is_castling(Color color);
    void make_castling();

    int is_check(Color color);
    int is_threatened(Position pos, Color color);
    int is_mat(Color color);

  private:
    /*!
    ** 8x8 matrix representing the chessboard
    */
    PAIR* chessboard_[DIM][DIM];
    std::list<Position> white_positions_;
    std::list<Position> black_positions_;
    std::stack<PAIR*> piece_cemetery_;
    std::stack<SexyMove*> moves_;
    size_t ever_moved_;
};

#endif /* CHESSBOARD_HH */
