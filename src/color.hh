#ifndef COMMON_COLOR_HH
# define COMMON_COLOR_HH

/*!
** \brief Represent the different piece colors present on the chessboard.
*/
enum Color
{
  WHITE,
  BLACK
};

#endif // !COMMON_COLOR_HH
