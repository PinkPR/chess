#ifndef ENEMY_SCORE_FEATURE_HH
# define ENEMY_SCORE_FEATURE_HH

# include "feature.hh"
# include "../color.hh"

# include <cmath>

/* Evaluate the score of the enemy */
class EnemyScoreFeature : public Feature
{
  public:
    EnemyScoreFeature();
    ~EnemyScoreFeature();
    float compute(Chessboard& chessboard, Color& color) override;

  private:
    float values_[6];
};

#endif /* !ENEMY_SCORE_FEATURE_HH */
