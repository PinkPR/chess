#include "feature-extractor.hh"

#define NB_FEATURE 1

FeatureExtractor::FeatureExtractor()
{
}

FeatureExtractor::~FeatureExtractor()
{
  for (Feature* f : this->features_)
  {
    delete f;
  }
}

void FeatureExtractor::add_feature(Feature* feature)
{
  this->features_.push_back(feature);
}

std::vector<float> FeatureExtractor::extract(Chessboard& chessboard,
                                             Color& color)
{
  std::vector<float> features;
  for (Feature* f : this->features_)
  {
    features.push_back(f->compute(chessboard, color));
  }
  return features;
}
