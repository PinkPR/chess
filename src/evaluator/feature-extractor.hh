#ifndef FEATURE_EXTRACTOR_HH
# define FEATURE_EXTRACTOR_HH
# include <vector>
# include <list>
# include <memory>
# include "feature.hh"
# include "../chessboard.hh"

class FeatureExtractor
{
  public:
    FeatureExtractor();
    ~FeatureExtractor();
    std::vector<float> extract(Chessboard& chessboard, Color& color);
    void add_feature(Feature* feature);
  private:
    std::list<Feature*> features_;
};

#endif /* !FEATURE_EXTRACTOR_HH */
