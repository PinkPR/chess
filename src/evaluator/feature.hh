#ifndef FEATURE_HH
# define FEATURE_HH
# include "../chessboard.hh"
# include "../color.hh"

class Feature
{
  public:
    virtual ~Feature();
    virtual float compute(Chessboard& chessboard, Color& color) = 0;
};

#endif /* !FEATURE_HH */
