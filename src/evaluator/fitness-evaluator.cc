#include "fitness-evaluator.hh"
#include "score-feature.hh"
#include "matrix-feature.hh"

FitnessEvaluator::FitnessEvaluator()
    : extractor_(new FeatureExtractor)
{
    extractor_->add_feature(new ScoreFeature());
    extractor_->add_feature(new ThreatFeature());
    extractor_->add_feature(new MatrixFeature());

    costs_[0] = 1.0f;
    costs_[1] = 0.3f;
    costs_[2] = 2.0f;
}

float FitnessEvaluator::compute_fitness(Chessboard& chessboard,
                                        Color& color)

{
    int i = 0;
    float res = 0.;

    /*
    ** This fonction compute a weighted sum at the moment, it will
    ** probably be a classification followed with a weigthed sum
    ** */

    //boost::timer::auto_cpu_timer t;

    std::vector<float> features = extractor_->extract(chessboard, color);
    for (float feature : features)
    {
        res += feature * costs_[i];
        i++;
    }
    return res;
}
