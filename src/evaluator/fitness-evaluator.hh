#ifndef FEATURE_EVALUATOR_HH
# define FEATURE_EVALUATOR_HH
# include "../color.hh"
# include "../chessboard.hh"
# include "feature-extractor.hh"
# include "threat-feature.hh"

class FitnessEvaluator
{
    public:
        FitnessEvaluator();

        float compute_fitness(Chessboard& chessboard, Color& color);

    private:
        std::shared_ptr<FeatureExtractor> extractor_;
        float costs_[3];

};

#endif /* !FEATURE_EVALUATOR_HH */
