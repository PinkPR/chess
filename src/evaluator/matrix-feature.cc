#include "matrix-feature.hh"

#include "../maps.hh"

MatrixFeature::MatrixFeature()
{
}

MatrixFeature::~MatrixFeature()
{
}

float MatrixFeature::compute(Chessboard& board, Color& color)
{
  float res = 0.0f;
  int pos = 0;
  PAIR* p = nullptr;

  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      p = board.get_piece(i, j);

      pos = 8 * (j - 1) + (i - 1);

      if (color == BLACK)
        pos = 63 - pos;

      if (p && p->second == color)
      {
        if (p->first == PAWN)
        {
          res += pawn_table_[pos];
        }
        else if (p->first == KNIGHT)
        {
          res += knight_table_[pos];
        }
        else if (p->first == BISHOP)
        {
          res += bishop_table_[pos];
        }
        else if (p->first == KING)
        {
          res += king_table_[pos];
        }
      }
    }
  }

  return res;
}
