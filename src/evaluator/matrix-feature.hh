#ifndef MATRIX_FEATURE_HH
# define MATRIX_FEATURE_HH

# include "feature.hh"


class MatrixFeature : public Feature
{
  public:
    MatrixFeature();
    ~MatrixFeature();

    virtual float compute(Chessboard& board, Color& color) override;
};

#endif /* MATRIX_FEATURE_HH */
