#include "score-feature.hh"
#include <iostream>

ScoreFeature::ScoreFeature()
{
  values_[0] = 32000.0f;
  values_[1] = 975.0f;
  values_[2] = 500.0f;
  values_[3] = 325.0f;
  values_[4] = 320.0f;
  values_[5] = 100.0f;
}

ScoreFeature::~ScoreFeature()
{
}

float ScoreFeature::compute(Chessboard& chessboard, Color& color)
{
   int res = 0;
    for (size_t x = 1; x <= 8; ++x)
    {
      for (size_t y = 1; y <= 8; ++y)
      {
        std::pair<PieceType, Color>* p = chessboard.get_piece(x, y);
        if (p && p->second == color)
        {
          res += values_[p->first];
        }
      }
    }
    return res;
}
