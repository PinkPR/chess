#ifndef SCORE_FEATURE_HH
# define SCORE_FEATURE_HH
# include "feature.hh"
# include "../color.hh"

# include <cmath>

/* Evaluate the player score */
class ScoreFeature : public Feature
{
  public:
    ScoreFeature();
    ~ScoreFeature();
    float compute(Chessboard& chessboard, Color& color) override;

  private:
    float values_[6];
};

#endif /* !SCORE_FEATURE_HH */
