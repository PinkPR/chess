#include "threat-feature.hh"

ThreatFeature::ThreatFeature()
{
    this->values_[0] = 32000;
    this->values_[1] = 975;
    this->values_[2] = 500;
    this->values_[3] = 325;
    this->values_[4] = 320;
    this->values_[5] = 100;
}

ThreatFeature::~ThreatFeature()
{
}

float ThreatFeature::compute(Chessboard& board, Color& color)
{
    float res1 = 0.0f;
    float res2 = 0.0f;

    //boost::thread t1(comp, this, board, color, 1, 4, res1);
    //boost::thread t2(comp, this, board, color, 5, 8, res2);

    comp(this, board, color, 1, 4, res1);
    comp(this, board, color, 5, 8, res2);

    //t1.join();
    //t2.join();

    return res1 + res2;
}

void  comp(ThreatFeature* obj, Chessboard& board, Color color,
           int x, int y, float& res)
{
    for (int i = x; i <= y; i++)
    {
        for (int j = 1; j <= 8; j++)
        {
            if (board.get_piece(i, j)
                && board.get_piece(i, j)->second == color)
            {
                res += obj->mine(board, color, i, j);
            }
        }
    }
}

int ThreatFeature::mine(Chessboard& board,
                        Color& color,
                        int x,
                        int y)
{
    int res = 0;
    Position orig(ST_FILE(x), ST_RANK(y));
    Position dest;
    Move move;

    for (int i = 1; i <= 8; i++)
    {
        for (int j = 1; j <= 8; j++)
        {
            if (board.get_piece(i, j)
                && board.get_piece(i, j)->second != color)
            {
                dest = Position(ST_FILE(i), ST_RANK(j));
                move = Move(orig, dest);

                if (allowed::allowed_move(move, color, board))
                {
                    res += this->values_[board.get_piece(i, j)->first];
                }
            }
        }
    }

    return res;
}
