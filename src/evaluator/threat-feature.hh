#ifndef THREAT_FEATURE_HH
# define THREAT_FEATURE_HH

# include "feature.hh"
# include "../allowed-moves.hh"

class ThreatFeature : public Feature
{
  public:
    ThreatFeature();
    ~ThreatFeature();

    virtual float compute(Chessboard& board, Color& color) override;
    int mine(Chessboard& board, Color& color, int x, int y);

  private:
    int values_[6];
};

void  comp(ThreatFeature* obj, Chessboard& board, Color color, int x,
           int y, float& res);

#endif /* THREAT_FEATURE_HH */
