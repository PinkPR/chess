#include <iostream>
#include <sstream>
#include <vector>

#include "game.hh"
#include "pgn/tag.hh"
#include "pgn/move_list.hh"
#include "pgn/parser.hh"

Game::Game()
{
  this->date_ = "????.??.??";
}

Game::Game(Player* p1, Player* p2)
{
  this->white_ = p1;
  this->black_ = p2;

  this->board_ = new Chessboard();
}

Game::Game(PgnPlayer* p1, PgnPlayer* p2)
{
  this->white_ = p1;
  this->black_ = p2;

  this->date_ = "????.??.??";

  this->board_ = new Chessboard();
}

Game::Game(PgnPlayer* p1, PgnPlayer* p2, std::vector<pgn::Tag> tags)
{
  this->white_ = p1;
  this->black_ = p2;
  this->date_ = "????.??.??";
  this->board_ = new Chessboard();
  this->tags_ = tags;
}

Game::Game(Human* p1, Human* p2)
{
  this->white_ = p1;
  this->black_ = p2;

  this->board_ = new Chessboard();
}

Game::Game(const std::vector<pgn::Tag>& tl, const pgn::MoveList& ml,
           const std::string& gr)
{
  tags_ = tl;
  moves_ = ml;
  result_ = gr;

  date_ = "????.??.??";
  this->white_ = new PgnPlayer(WHITE);
  this->black_ = new PgnPlayer(BLACK);

  for (std::vector<pgn::Tag>::iterator it = tags_.begin();
       it != tags_.end(); ++it)
  {
    if (it->name() == "Date")
      date_ = it->value();

    if (it->name() == "White")
      static_cast<PgnPlayer*>(this->white_)->set_name(it->value());

    if (it->name() == "Black")
      static_cast<PgnPlayer*>(this->black_)->set_name(it->value());
  }
}

Game::Game(const Game& src)
{
  tags_ = src.tags_;
  moves_ = src.moves_;
  result_ = src.result_;
  date_ = src.date_;
  white_ = src.white_;
  black_ = src.black_;
}

Game::Game(std::istream& is)
{
  is >> *this;
}

Game::~Game()
{
}

Game&
Game::operator=(const Game& src)
{
  if (&src == this)
    return(*this);

  tags_ = src.tags_;
  moves_ = src.moves_;
  result_ = src.result_;
  date_ = src.date_;
  white_ = src.white_;
  black_ = src.black_;

  return *this;
}

bool
Game::operator==(const Game& src) const
{
  if (tags_ != src.tags_)
    return false;
  if (moves_ != src.moves_)
    return false;
  if (result_ != src.result_)
    return false;
  if (date_ != src.date_)
    return false;
  if (white_ != src.white_)
    return false;
  if (black_ != src.black_)
    return false;

  return true;
}

bool
Game::operator!=(const Game& src) const
{
  return !(*this == src);
}

std::ostream&
operator<<(std::ostream& os, const Game& src)
{
  for (std::vector<pgn::Tag>::const_iterator it = src.tags_.begin();
       it != src.tags_.end(); ++it)
    os << (*it) << std::endl;
  os << std::endl;

  if (src.moves_.size())
    os << src.moves_;

  os << src.result_;
  os << std::endl;

  return os;
}

void
Game::set_listener(Listener* listener)
{
  this->listener_ = listener;
}

std::istream&
operator>>(std::istream& is, Game& src)
{
  std::string pgndata;
  std::copy(std::istreambuf_iterator<char>(is),
            std::istreambuf_iterator<char>(),
            std::inserter(pgndata, pgndata.end()));

  std::string::const_iterator itr1 = pgndata.begin();
  std::string::const_iterator itr2 = pgndata.end();

  pgn::Parser parser;
  parser.getGame(itr1, itr2, src);
  src.board_ = new Chessboard();
  return is;
}

const std::vector<pgn::Tag>&
Game::tags() const
{
  return tags_;
}

const pgn::MoveList&
Game::moves() const
{
  return moves_;
}

std::string
Game::date() const
{
  return date_;
}

Player*
Game::white()
{
  return white_;
}

Player*
Game::black()
{
  return black_;
}

std::string
Game::result() const
{
  return result_;
}

bool
Game::isWhiteWin() const
{
  return result_ == "1-0";
}

bool
Game::isBlackWin() const
{
  return result_ == "0-1";
}

bool
Game::isDrawn() const
{
  return result_ == "1/2-1/2";
}

bool
Game::isUnknown() const
{
  return result_ == "*";
}

//   (
//     on_piece_taken?
//     (
//       (
//         on_{queen,king}side_castling
//         on_piece_moved /* king move */
//         on_piece_moved /* rook */
//       )? |
//XX       on_piece_moved
//     )
//     on_piece_promoted?
//     (
//       on_player_mat |
//       on_player_check |
//       on_player_pat on_draw
//     )?
//   ) |
//   on_draw |
//   on_player_disqualified |
//   on_player_timeout
void Game::run()
{
  //Color c1 = WHITE;
  //Color c2 = BLACK;

  //this->listener_->register_chessboard_adapter(*this->board_);
  //this->listener_->on_game_started();

  std::string lol = "";

  while (1)
  {
//# if DEBUG
    chessboard_printer::print(*(this->board_));
    std::cin >> lol;
//# endif /* !DEBUG */
    this->last_move_ = this->white_->move_get();

    //while (!allowed::allowed_move(this->last_move_,
      //                            c1,
        //                          *(this->board_)))
    //{
      //this->last_move_ = this->white_->move_get();
    //}
    if (!allowed::allowed_move(last_move_, white_->color_get(),
                               *board_))
    {
      lol = " ";
    }

    this->board_->make_move(this->last_move_, WHITE);

    /*PieceType p = this->board_->get_piece(static_cast<int>(
          this->last_move_.end_get().file_get()),
          static_cast<int>(this->last_move_.end_get().rank_get()))->first;*/
    //this->listener_->on_piece_moved(p, this->last_move_.start_get(),
      //                              this->last_move_.end_get());

    this->black_->last_opponent_move_set(this->last_move_);

//# if DEBUG
    chessboard_printer::print(*(this->board_));
    std::cin >> lol;
//# endif /* !DEBUG */
    //this->last_move_ = this->black_->move_get();

    //while (!allowed::allowed_move(this->last_move_,
      //                            c2,
        //                          *(this->board_)))
    //{
      this->last_move_ = this->black_->move_get();
    //}

    this->board_->make_move(this->last_move_, BLACK);
    /*p = this->board_->get_piece(static_cast<int>(
          this->last_move_.end_get().file_get()),
          static_cast<int>(this->last_move_.end_get().rank_get()))->first;*/
    //this->listener_->on_piece_moved(p, this->last_move_.start_get(),
      //                              this->last_move_.end_get());
    this->white_->last_opponent_move_set(this->last_move_);
  }
  //this->listener_->on_game_finished();
}
