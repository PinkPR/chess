#ifndef game_hh
# define game_hh

# include <fstream>
# include <vector>
# include <string>

# include "pgn/tag.hh"
# include "pgn/comment.hh"
# include "pgn/move_list.hh"
# include "player.hh"
# include "pgn-player.hh"
# include "chessboard-printer.hh"
# include "chessboard-adapter.hh"
# include "chessboard.hh"
# include "human.hh"
# include "allowed-moves.hh"
# include "listener.hh"

class Game
{
public:
  Game();
  Game(Player* p1, Player* p2);
  Game(PgnPlayer* p1, PgnPlayer* p2);
  Game(Human* p1, Human* p2);
  Game(PgnPlayer* p1, PgnPlayer* p2, std::vector<pgn::Tag> tags);
  Game(const std::vector<pgn::Tag>& tl, const pgn::MoveList& ml,
       const std::string& gr);
  Game(const Game& src);
  Game(std::istream& is);
  ~Game();

  Game& operator=(const Game& src);
  bool operator==(const Game& src) const;
  bool operator!=(const Game& src) const;

  void set_listener(Listener* listener);

  const std::vector<pgn::Tag>& tags() const;
  const pgn::MoveList& moves() const;
  std::string date() const;
  Player* white();
  Player* black();
  std::string result() const;

  bool isWhiteWin() const;
  bool isBlackWin() const;
  bool isDrawn() const;
  bool isUnknown() const;

  friend std::ostream& operator<<(std::ostream& os, const Game& src);
  friend std::istream& operator>>(std::istream& is, Game& src);
  void run();

private:
  std::vector<pgn::Tag> tags_;
  pgn::MoveList moves_;
  std::string result_;
  std::string date_;
  Player* white_;
  Player* black_;
  Chessboard* board_;
  Move last_move_;
  Listener* listener_;
};

#endif /* !game_hh */
