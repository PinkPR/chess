#include "human.hh"

Human::Human(Color color)
      : Player(color)
{
}

Human::Human(Color color, std::string name)
      : Player(color)
{
  this->set_name(name);
}

Human::~Human()
{
}

Color Human::get_color()
{
  return this->color_;
}

void Human::set_board(Chessboard* board)
{
  this->board_ = board;
}

void 
Human::last_opponent_move_set(const Move& last_opponent_move)
{
  this->last_opponent_move_ = last_opponent_move;

  /*Position orig = last_opponent_move.start_get();
  Position dest = last_opponent_move.end_get();
  std::pair<PieceType, Color> p =
  (*board_)[last_opponent_move.start_get()];

  board_->add_piece(p.first, p.second, dest);
  board_->add_piece(NONE, BLACK, orig);*/
}

Move Human::move_get()
{
  std::string input = "";

  std::cout << "Piece to move : ";
  std::cin >> input;
  const Position orig = this->parse_position(input.c_str());

  std::cout << std::endl;
  std::cout << "Destination : ";
  std::cin >> input;
  const Position dest = this->parse_position(input.c_str());

  return Move(orig, dest);
}

std::string Human::get_name()
{
  return name_;
}

void Human::set_name(std::string name)
{
  name_ = name;
}

Position Human::parse_position(const char* str)
{
  int file = str[0] - 'a' + 1;
  int rank = str[1] - '0';

  return Position(ST_FILE(file), ST_RANK(rank));
}
