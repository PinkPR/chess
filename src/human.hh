#ifndef HUMAN_HH
# define HUMAN_HH

# include <iostream>
# include <string>

# include "player.hh"
# include "chessboard.hh"

class Human : public Player
{
  public:
    Human(Color color);
    Human(Color color, std::string name);
    ~Human() override;

  public:
    void
    last_opponent_move_set(const Move& last_opponent_move);
    Move move_get() override;
    void set_board(Chessboard* board);
    Color get_color();

    std::string get_name();
    void set_name(std::string name);

  private:
    Position parse_position(const char* str);

  protected:
    Chessboard* board_;
    std::string name_;
};

#endif /* HUMAN_HH */
