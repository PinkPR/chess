#include "game.hh"
#include "human.hh"
#include "ai-dummy.hh"
#include "best-ai-ever.hh"

int main()
{
  /*Game* game = new Game(new Human(WHITE), new Human(BLACK));
  game->run();*/

//  Human* human = new Human(WHITE);
  BestAiEver* ai = new BestAiEver(WHITE);
  BestAiEver* ai2 = new BestAiEver(BLACK);

  Game* game = new Game(ai, ai2);
  game->run();

  return 0;
}
