#include "minimax.hh"
#include <iostream>
#include "chessboard-printer.hh"

MiniMax::MiniMax()
{

}

Move MiniMax::operator()(Chessboard& chessboard, Color color)
{
    Color oponent = color == WHITE ? BLACK : WHITE;
    Move move = this->rec_minimax(chessboard, color, oponent, 0).first;

    return move;
}

std::pair<Move, float> MiniMax::rec_minimax(Chessboard& chessboard,
                                            Color& player,
                                            Color& oponent,
                                            int n)
{
    if (n >= 3)
    {
        float fit;
        fit = this->evaluator_.compute_fitness(chessboard, player);
        fit -= this->evaluator_.compute_fitness(chessboard, oponent);

        return std::pair<Move, float>(Move(), fit);
    }

    std::vector<std::pair<Move, float>> choices;
    std::pair<Move, float> best(Move(), -INFINITY);

    for (Move move : chessboard.get_allowed_moves(player))
    {
        chessboard.make_move(move, player);
        std::pair<Move, float> s(move,
                                 rec_minimax(chessboard,
                                             oponent,
                                             player,
                                             n + 1).second);
        choices.push_back(s);
        chessboard.revert_move();
    }

    if (choices.size() == 0)
        return best;

    for (std::pair<Move, float> choice : choices)
    {
        if (choice.second > best.second)
            best = choice;
    }

    return best;
}
