#ifndef MINIMAX_HH
# define MINIMAX_HH
# include <cmath>
# include "move.hh"
# include "chessboard.hh"
# include "color.hh"
# include "evaluator/fitness-evaluator.hh"

class MiniMax
{
    public:
        MiniMax();
        Move operator()(Chessboard& chessboard, Color player);
    private:
        std::pair<Move, float> rec_minimax(Chessboard& chessboard,
                                           Color& champion,
                                           Color& oponent,
                                           int n = 0);
        FitnessEvaluator evaluator_;
};

#endif /* !MINIMAX_HH */
