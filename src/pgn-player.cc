#include "pgn-player.hh"

PgnPlayer::PgnPlayer(Color color)
      : Player(color)
{
}

PgnPlayer::~PgnPlayer()
{
}

Color PgnPlayer::get_color()
{
  return this->color_;
}

void
PgnPlayer::last_opponent_move_set(const Move& move)
{
  this->last_opponent_move_ = move;

  Position orig = move.start_get();
  Position dest = move.end_get();

  PAIR* piece =
    this->board_.get_piece(orig.file_get(), orig.rank_get());
  this->board_.set_piece(piece, dest.file_get(), dest.rank_get());
  this->board_.set_piece(nullptr, dest.file_get(), dest.rank_get());
}

Move
PgnPlayer::move_get()
{
  SexyMove smove = this->moves_.front();
  this->moves_.pop();
  Move move = smove.get_move();

//# if DEBUG
  std::cout << "Piece : " << (int) smove.get_eaten() << std::endl;

  std::cout << " Orig : " << (int) move.start_get().file_get();
  std::cout << " " << (int) move.start_get().rank_get() << std::endl;
  std::cout << " Dest : " << (int) move.end_get().file_get();
  std::cout << " " << (int) move.end_get().rank_get() << std::endl;
//# endif /* !DEBUG */

  Move fmove = this->reverse_move(smove);

//# if DEBUG
  std::cout << "Piece : " << this->board_[fmove.start_get()].first <<
  std::endl;
  std::cout << " Orig : " << (int) fmove.start_get().file_get();
  std::cout << " " << (int) fmove.start_get().rank_get() << std::endl;
  std::cout << " Dest : " << (int) fmove.end_get().file_get();
  std::cout << " " << (int) fmove.end_get().rank_get() << std::endl;
//# endif /* !DEBUG */

  this->board_.make_move(fmove, this->color_);

  return fmove;
}

std::string
PgnPlayer::get_name()
{
  return name_;
}

void
PgnPlayer::set_name(std::string name)
{
  name_ = name;
}

std::queue<SexyMove>&
PgnPlayer::get_moves()
{
  return this->moves_;
}

Move
PgnPlayer::reverse_move(SexyMove sexmove)
{
  Move move = sexmove.get_move();
  Position orig = move.start_get();
  Position dest = move.end_get();

  for (int i = 1; i <= 8; i++)
  {
    for (int j = 1; j <= 8; j++)
    {
      if (this->board_.get_piece(i, j)
          && this->board_.get_piece(i, j)->first ==
          sexmove.get_eaten()
          && this->board_.get_piece(i, j)->second == this->color_)
      {
        orig = Position(static_cast<Position::File>(i),
                        static_cast<Position::Rank>(j));

        if (allowed::allowed_move(*(new Move(orig, dest)),
                                  this->color_,
                                  this->board_))
        {
          return Move(orig, dest);
        }
      }
    }
  }

  return move;
}
