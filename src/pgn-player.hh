#ifndef PGN_PLAYER_HH
# define PGN_PLAYER_HH

# include <iostream>
# include <string>
# include <queue>

# include "player.hh"
# include "sexy-move.hh"
# include "chessboard.hh"
# include "allowed-moves.hh"

class PgnPlayer : public Player
{
  public:
    PgnPlayer(Color color);
    ~PgnPlayer() override;

  public:
    void last_opponent_move_set(const Move& last_opponent_move);
    Move move_get() override;
    Color get_color();

    std::string get_name();
    void set_name(std::string name);
    std::queue<SexyMove>& get_moves();

    Move reverse_move(SexyMove sexmove);

  protected:
    std::string name_;
    std::queue<SexyMove> moves_;
    Chessboard board_;
};

#endif /* PGN_PLAYER_HH */
