#include <sstream>

#include "comment.hh"

pgn::Comment::Comment()
{
}

pgn::Comment::Comment(const std::string& text)
{
  comment_ = text;
}

pgn::Comment::Comment(const pgn::Comment& src)
{
  comment_ = src.comment_;
}

pgn::Comment::~Comment()
{
}

pgn::Comment&
pgn::Comment::operator=(const pgn::Comment& src)
{
  if (&src == this)
    return(*this);

  comment_ = src.comment_;
  return *this;
}

bool
pgn::Comment::operator==(const pgn::Comment& src) const
{
  return comment_ == src.comment_;
}

bool
pgn::Comment::operator!=(const pgn::Comment& src) const
{
  return !(*this == src);
}

std::ostream&
pgn::operator<<(std::ostream& os, const pgn::Comment& src)
{
  if (src.comment_ == "")
    return os;

  os << " {" << src.comment_ << "}";
  return os;
}
