#ifndef comment_hh
# define comment_hh

# include <fstream>
# include <string>

namespace pgn
{
  class Comment
  {
  public:
    Comment();
    Comment(const std::string &s);
    Comment(const Comment& src);
    ~Comment();

    Comment& operator=(const Comment& src);
    bool operator==(const Comment& src) const;
    bool operator!=(const Comment& src) const;

    friend std::ostream& operator<<( std::ostream& os, const Comment& src);

  private:
    std::string comment_;
  };

  std::ostream& operator<<(std::ostream& os, const pgn::Comment& src);
};

# endif /* !comment_hh */
