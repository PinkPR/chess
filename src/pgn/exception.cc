#include "exception.hh"

pgn::invalid_castle_string::invalid_castle_string() throw()
  :pgn::parse_exception("invalid string for castle (maybe 0 (zero) used instead of 'O')")
{
}

pgn::invalid_ply_text::invalid_ply_text(const std::string &ply) throw()
  :pgn::parse_exception("invalid text in ply >" + ply + "<")
{
}

pgn::invalid_tag::invalid_tag(const std::string &tag) throw()
  :pgn::parse_exception("invalid text in tag >" + tag + "<")
{
}

pgn::invalid_result::invalid_result() throw()
  :pgn::parse_exception("invalid result")
{
}

pgn::missing_result::missing_result() throw()
  :pgn::parse_exception("missing result")
{
}

pgn::unsupported_variations::unsupported_variations() throw()
  :pgn::parse_exception("support for variations not yet available in pgnlib")
{
}

pgn::unsupported_glyphs::unsupported_glyphs() throw()
  :pgn::parse_exception("support for glyphs not yet available in pgnlib")
{
}

pgn::invalid_fen_string::invalid_fen_string(const std::string &fenstring) throw()
  :message_("invalid characters in fen string >" + fenstring + "<")
{
}
