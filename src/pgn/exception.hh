#ifndef exception_hh
# define exception_hh

# include <exception>
# include <string>

namespace pgn
{
  class parse_exception : public std::exception
  {
    public:
      parse_exception(const std::string &message) throw():message_(message) {}
      ~parse_exception() throw() {}
      void bindParsingText(const std::string &parsingtext) throw() { parsing_text_ = parsingtext; }
      const char *parsing_text() const throw() { return parsing_text_.c_str(); }
      virtual const char *what() const throw() { return message_.c_str(); }

    private:
      std::string message_;
      std::string parsing_text_;
  };

  class invalid_castle_string : public parse_exception
  {
    public:
      invalid_castle_string() throw();
      ~invalid_castle_string() throw() {};
  };

  class invalid_ply_text : public parse_exception
  {
    public:
      ~invalid_ply_text() throw() {};
      invalid_ply_text(const std::string &ply) throw();
  };

  class invalid_tag : public parse_exception
  {
    public:
      ~invalid_tag() throw() {};
      invalid_tag(const std::string &tag) throw();
  };

  class invalid_result : public parse_exception
  {
    public:
      ~invalid_result() throw() {};
      invalid_result() throw();
  };

  class missing_result : public parse_exception
  {
    public:
      ~missing_result() throw() {};
      missing_result() throw();
  };

  class unsupported_variations : public parse_exception
  {
    public:
      ~unsupported_variations() throw() {};
      unsupported_variations() throw();
  };

  class unsupported_glyphs : public parse_exception
  {
    public:
      ~unsupported_glyphs() throw() {};
      unsupported_glyphs() throw();
  };

  class invalid_fen_string : public std::exception
  {
    public:
      invalid_fen_string(const std::string &fenstring) throw();
      ~invalid_fen_string() throw() {}

      virtual const char *what() const throw() { return message_.c_str(); }

    private:
      std::string message_;
  };
}

#endif /* !exception_hh */
