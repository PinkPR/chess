#include <iostream>
#include <sstream>
#include <stdexcept>

#include "move.hh"
#include "parser.hh"

pgn::Move::Move()
    :complete_(false), number_(0)
{
}

pgn::Move::Move(const pgn::Move& src)
    :complete_(src.complete_), number_(src.number_), white_(src.white_),
     black_(src.black_)
{
}

pgn::Move& pgn::Move::operator=(const pgn::Move& src)
{
  if (&src == this)
    return(*this);

  complete_ = src.complete_;
  number_ = src.number_;
  white_ = src.white_;
  black_ = src.black_;

  return *this;
}

pgn::Move::Move(const pgn::Ply &white, const pgn::Ply &black, int number)
{
  complete_ = (white_.valid() && black_.valid());
  number_ = number;
  white_ = white;
  black_ = black;
}

pgn::Move::~Move()
{
}

bool
pgn::Move::operator==(const pgn::Move& src) const
{
  if (complete_ != src.complete_)
    return false;
  if (number_ != src.number_)
    return false;
  if (white_ != src.white_)
    return false;
  if (black_ != src.black_)
    return false;

  return true;
}

std::ostream&
pgn::operator<<(std::ostream& os, const pgn::Move& src)
{
  if (src.complete_)
    os << src.number_ << ". " << src.white_ << " " << src.black_;
  else if (src.white_.valid())
    os << src.number_ << ". " << src.white_;
  else if (src.black_.valid())
    os << src.number_ << "... " << src.black_;
  return os;
}

std::istream&
pgn::operator>>(std::istream& is, pgn::Move& src)
{
  std::string str;
  std::copy(std::istreambuf_iterator<char>(is), std::istreambuf_iterator<char>(), std::inserter(str, str.end()));
  pgn::Parser parser;
  std::string::const_iterator itr1 = str.begin();
  std::string::const_iterator itr2 = str.end();
  parser.getMove(itr1, itr2, src);

  return is;
}

bool
pgn::Move::isCheckMate() const
{
  return white_.isCheckMate() || black_.isCheckMate();
}

bool
pgn::Move::operator!=(const pgn::Move& src) const
{
  return !(*this == src);
}

pgn::Ply
pgn::Move::white() const
{
  return white_;
}

pgn::Ply
pgn::Move::black() const
{
  return black_;
}
