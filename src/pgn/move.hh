#ifndef move_hh
# define move_hh

# include <fstream>

# include "ply.hh"

namespace pgn
{
  class Move
  {
	public:
		Move();
		Move(const Move& src);
		Move(const Ply &white, const Ply &black, int number);
		~Move();

		Move& operator=(const Move& src);
		bool operator==(const Move& src) const;
		bool operator!=(const Move& src) const;

		bool isCheckMate() const;
		Ply white() const;
		Ply black() const;

		friend std::istream& operator>>(std::istream& is, Move& src);
		friend std::ostream& operator<<(std::ostream& os, const Move& src);

	private:
    bool complete_;
    int number_;
    pgn::Ply white_;
    pgn::Ply black_;
  };

  std::istream& operator>>(std::istream& is, Move& src);
  std::ostream& operator<<(std::ostream& os, const Move& src);
};

#endif /* !move_hh */
