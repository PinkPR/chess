#ifndef move_list_hh
# define move_list_hh

# include <fstream>
# include <vector>
# include <set>

# include "move.hh"

namespace pgn
{
  class MoveList 
  {
    public:
      class iterator;
      friend class iterator;

      MoveList();
      MoveList(const MoveList& src);

      virtual ~MoveList();

      MoveList& operator = (const MoveList& src);
      bool operator == (const MoveList& src) const;
      bool operator != (const MoveList& src) const;
      Move operator [] (int idx);

      void insert(const Move& src);
      bool find(const Move& src) const;
      int size() const;
      iterator begin() const;
      iterator end() const;
      void bindComment(const Comment &ct);

      friend std::ostream& operator << ( std::ostream& os, const MoveList& src);
      friend std::istream& operator >> ( std::istream& is, MoveList& src);

    private:
      struct MoveListData *hdata;
  };


  class MoveList::iterator
  {
    public:
      typedef std::forward_iterator_tag iterator_category;
      typedef Move value_type;
      typedef Move* pointer;
      typedef Move& reference;
      typedef int difference_type;

      iterator();
      iterator(const MoveList &ml);
      iterator(const MoveList &ml, int);
      iterator(const iterator&);
      ~iterator();
      iterator& operator = (const iterator&);
      iterator& operator ++ ();
      iterator& operator ++ (int);
      pointer operator -> () const;
      reference operator * () const;
      bool operator == (const iterator&) const;
      bool operator != (const iterator&) const;

    private:
      struct iteratorData *hdata;
  };
};

#endif /* !move_list_hh */
