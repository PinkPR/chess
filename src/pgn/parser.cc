#include "parser.hh"

bool
pgn::Parser::getGame(std::string::const_iterator &itr1,
                     const std::string::const_iterator &itr2, Game &out)
{
  std::string::const_iterator start_game_itr = itr1;
  std::string::const_iterator local_itr = itr1;

  try
  {
    std::vector<pgn::Tag> taglist;

    if (!getTagList(local_itr, itr2, taglist))
      return 0;

    itr1 = local_itr;
    skipBlanks(local_itr, itr2);
    pgn::Comment bgComment;
    bool hasBgComment = false;

    if (getComment(local_itr, itr2, bgComment))
    {
      hasBgComment = true;
      itr1 = local_itr;
    }

    skipBlanks(local_itr, itr2);
    pgn::MoveList  movelist;

    if (!getMoveList(local_itr, itr2, movelist))
      throw std::runtime_error("Error parsing movelist");

    itr1 = local_itr;
    std::string gameResult;

    if (!getGameResult(local_itr, itr2, gameResult))
      return 0;

    itr1 = local_itr;
    out = Game(taglist, movelist, gameResult);
    skipBlanks(local_itr, itr2);
    pgn::Comment agComment;
    bool hasAgComment = false;

    if (getComment(local_itr, itr2, agComment))
    {
      hasAgComment = true;
      itr1 = local_itr;
    }

    /*
     * From here start the ridiculous adapter to convert the parser moves to
     * sexy-moves
     */
    int count  = 1;
    for (pgn::MoveList::iterator it = out.moves().begin();
        it != out.moves().end(); ++it)
    {
      int col = 1;
      int row = 1;
      int tr = 0;
      PieceType p_type = PAWN;

      if (it->white().piece().letter() == 'N')
        p_type = KNIGHT;
      else if (it->white().piece().letter() == 'B')
        p_type = BISHOP;
      else if (it->white().piece().letter() == 'R')
        p_type = ROOK;
      else if (it->white().piece().letter() == 'Q')
        p_type = QUEEN;
      else if (it->white().piece().letter() == 'K')
        p_type = KING;

# if DEBUG
      std::cout << count << "." << std::endl;
      std::cout << "white moves : " << p_type << "(" << it->white().piece().letter() <<")" <<std::endl;

      if (it->white().isLongCastle())
        std::cout << "queen castle" << std::endl;
      if (it->white().isShortCastle())
        std::cout << "king castle" << std::endl;
# endif /* !DEBUG */


      if (it->white().str() != "O-O" && it->white().str() != "O-O-O")
      {
        col = it->white().toSquare().col() - 'a' + 1;
        row = it->white().toSquare().row() - '0';
      }
      else if (it->white().str() != "O-O")
        tr = KING_CASTLING;
      else
        tr = QUEEN_CASTLING;

      Position from = Position(static_cast<Position::File>(1),
          static_cast<Position::Rank>(1));
      Position to = Position(static_cast<Position::File>(col),
          static_cast<Position::Rank>(row));
      SexyMove move = SexyMove(::Move(from, to), p_type, tr);
      static_cast<PgnPlayer*>(out.white())->get_moves().push(move);

      if (it->black().str() != "")
      {
        int colb = 1;
        int rowb = 1;
        int trb = 0;
        PieceType p_typeb = PAWN;

        if (it->black().piece().letter() == 'N')
          p_typeb = KNIGHT;
        else if (it->black().piece().letter() == 'B')
          p_typeb = BISHOP;
        else if (it->black().piece().letter() == 'R')
          p_typeb = ROOK;
        else if (it->black().piece().letter() == 'Q')
          p_typeb = QUEEN;
        else if (it->black().piece().letter() == 'K')
          p_typeb = KING;

# if DEBUG
        std::cout << "black moves : " << p_typeb << "(" << it->black().piece().letter() <<") "<< std::endl;

        if (it->black().isLongCastle())
          std::cout << "queen castle" << std::endl;
        if (it->black().isShortCastle())
          std::cout << "king castle" << std::endl;
# endif /* !DEBUG */

        if (it->black().str() != "O-O" && it->black().str() != "O-O-O")
        {
          colb = it->black().toSquare().col() - 'a' + 1;
          rowb = it->black().toSquare().row() - '0';
        }
        else if (it->white().str() != "O-O")
          trb = KING_CASTLING;
        else
          trb = QUEEN_CASTLING;

        Position fromb = Position(static_cast<Position::File>(1),
            static_cast<Position::Rank>(1));
        Position tob = Position(static_cast<Position::File>(colb),
            static_cast<Position::Rank>(rowb));
        SexyMove moveb = SexyMove(::Move(fromb, tob), p_typeb, trb);
        static_cast<PgnPlayer*>(out.black())->get_moves().push(moveb);
      }
      count++;
    }

    return true;
  }
  catch (pgn::parse_exception &pe)
  {
    pe.bindParsingText(std::string(start_game_itr, local_itr));
    throw;
  }
  catch (std::exception &e)
  {
    std::ostringstream err;
    err << e.what() << ". I was parsing this game:"
      << std::string(start_game_itr, local_itr);
    throw std::runtime_error(err.str());
  }
  return 0;
}

  bool
pgn::Parser::getTagList(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2,
    std::vector<pgn::Tag> &out)
{
  std::string::const_iterator local_itr = itr1;
  std::vector<pgn::Tag> taglist;
  pgn::Tag tag;

  while (getTag(local_itr, itr2, tag))
  {
    itr1 = local_itr;
    taglist.push_back(tag);
  }

  if (taglist.size() == 0)
    return false;

  out = taglist;
  return true;
}

  bool
pgn::Parser::getTag(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2, pgn::Tag &out)
{
  std::string::const_iterator local_itr = itr1;
  std::string::const_iterator start_tag = itr1;

  // '['
  skipBlanks(local_itr, itr2);
  if (*local_itr != '[')
    return false;
  local_itr++;

  // '<tagname>'
  skipBlanks(local_itr, itr2);
  std::string tagname;
  while ((local_itr != itr2) && (isalnum(*local_itr)))
    tagname += *local_itr++;
  itr1 = local_itr;

  skipBlanks(local_itr, itr2);
  std::string tagvalue;
  while ((local_itr != itr2) && (*local_itr != ']'))
    tagvalue += *local_itr++;

  if (local_itr == itr2) // missing ']'
    throw pgn::invalid_tag(std::string(start_tag, local_itr));

  if (tagvalue[0] != '"' || tagvalue[tagvalue.size()-1] != '"')
    throw pgn::invalid_tag(std::string(start_tag, local_itr)); // missing '"'

  tagvalue = tagvalue.substr(1,tagvalue.size()-2);
  local_itr++;
  out = pgn::Tag(tagname, tagvalue);
  itr1 = local_itr;

  return true;
}

  bool
pgn::Parser::getMoveList(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2,
    pgn::MoveList &out)
{
  std::string::const_iterator local_itr = itr1;
  pgn::MoveList ml;
  pgn::Comment comment;

  if (getComment(local_itr, itr2, comment))
  {
    skipBlanks(local_itr, itr2);
    itr1 = local_itr;
    ml.bindComment(comment);
  }

  skipBlanks(local_itr, itr2);
  pgn::Move move;

  while ((getMove(local_itr, itr2, move)))
  {
    itr1 = local_itr;
    ml.insert(move);
  }

  itr1 = local_itr;
  out = ml;

  return true;
}

/**
 *
 * Possible forms:
 * <move_number><dots> <ply> <result>
 * <move_number><dots> <ply> <move_number>
 * <move_number><dots> <ply> <ply> <result>
 * <move_number><dots> <ply> <ply> <move_number>
 */
  bool
pgn::Parser::getMove(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2, pgn::Move &src)
{
  std::string::const_iterator local_itr = itr1;

  // looking for move number
  std::string movenumber;
  int dotsCount;
  if (! getMoveNumber(local_itr, itr2, movenumber, dotsCount))
    return false;
  itr1 = local_itr;

  // looking for first ply (mandatory)
  skipBlanks(local_itr, itr2);
  pgn::Ply firstPly;
  if (!getPly(local_itr, itr2, firstPly))
    throw std::runtime_error("Error parsing move");

  skipBlanks(local_itr, itr2);
  pgn::Ply secondPly;

  // eof
  if (local_itr == itr2)
  {
    src = pgn::Move(firstPly, secondPly, atoi(movenumber.c_str()));
    moveCount_++;
    return true;
  }

  // second ply, game result or another move number?
  // a ply always begins with an uppercase or lowercase alphabetic char.
  // digits and '*' have lesser ascii values.
  if (*local_itr > '9')
  {
    // looking for second ply (optional)
    getPly(local_itr, itr2, secondPly);
    skipBlanks(local_itr, itr2);
  }
  itr1 = local_itr;

  src = pgn::Move(firstPly, secondPly, atoi(movenumber.c_str()));
  moveCount_++;

  return true;
}

  bool
pgn::Parser::getGameResult(std::string::const_iterator& itr1,
    const std::string::const_iterator& itr2,
    std::string& out)
{
  std::string::const_iterator local_itr = itr1;
  std::string result;

  while (!isspace(*local_itr) && local_itr != itr2)
    result += *local_itr++;

  if (result[0] == '[')
    throw pgn::missing_result();
  else if ((result != "1-0") && (result != "0-1")
      && (result != "1/2-1/2") && (result != "*"))
    throw pgn::invalid_result();

  out = result;
  itr1 = local_itr;
  return true;
}

  bool
pgn::Parser::getMoveNumber(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2,
    std::string &out, int &dotsCount)
{
  std::string::const_iterator local_itr = itr1;

  if (!isdigit(*local_itr))
    return false;

  // looking for number
  std::string movenumber;
  while (isdigit(*local_itr) && local_itr != itr2)
    movenumber += *local_itr++;

  skipBlanks(local_itr, itr2);

  if (*local_itr != '.')
    return false;

  // skipping and counting dots
  dotsCount=0;
  while ((*local_itr == '.') && (local_itr != itr2))
  {
    local_itr++;
    dotsCount++;
  }

  itr1 = local_itr;
  out = movenumber;
  return true;
}

  bool
pgn::Parser::getPly(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2, pgn::Ply &out)
{
  std::string::const_iterator local_itr = itr1;
  skipBlanks(local_itr, itr2);

  while ((local_itr != itr2) && (!isspace(*local_itr)))
  {
    if (*local_itr != ')') // caso "Nf4)"
      local_itr++;
    else
      break;
  }

  out = pgn::Ply(std::string(itr1, local_itr));
  skipBlanks(local_itr, itr2);
  itr1 = local_itr;
  int glyphval = getGlyph(local_itr);

  if (glyphval >= 0)
  {
    skipBlanks(local_itr, itr2);
    itr1 = local_itr;
    out.setGlyphValue(glyphval);
  }

  itr1 = local_itr;
  pgn::Comment comment;

  if (getComment(local_itr, itr2, comment))
  {
    skipBlanks(local_itr, itr2);
    itr1 = local_itr;
    out.bindComment(comment);
  }

  itr1 = local_itr;
  pgn::MoveList variation;

  while (getVariation(local_itr, itr2, variation))
  {
    skipBlanks(local_itr, itr2);
    itr1 = local_itr;
    out.bindVariation(variation);
  }

  itr1 = local_itr;
  plyCount_++;
  return true;
}

  bool
pgn::Parser::getComment(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2,
    pgn::Comment &out)
{
  std::string::const_iterator local_itr = itr1;
  std::string comment;

  if (*local_itr != '{')
    return false;

  local_itr++; // skipping '{'

  while ((*local_itr != '}') && (local_itr != itr2))
    comment += *local_itr++;

  local_itr++; // skipping '}'
  skipBlanks(local_itr, itr2);
  itr1 = local_itr;
  out = pgn::Comment(comment);
  return true;
}

  bool
pgn::Parser::getVariation(std::string::const_iterator &itr1,
    const std::string::const_iterator &itr2,
    pgn::MoveList &out)
{
  std::string::const_iterator local_itr = itr1;
  if (*local_itr != '(')
    return false;
  local_itr++; // skipping '('

  if (!getMoveList(local_itr, itr2, out))
    throw std::runtime_error("Error parsing movelist");

  local_itr++; // skipping ')'
  skipBlanks(local_itr, itr2);
  itr1 = local_itr;

  return true;
}

  int
pgn::Parser::getGlyph(std::string::const_iterator &itr1)
{
  if (*itr1 == '$')
  {
    int val = -1;
    itr1++;
    if (isdigit(*itr1))
      val = *itr1 - '0';
    itr1++;
    if (isdigit(*itr1))
      val = val*10 + (*itr1 - '0');
    return val;
  }
  else
    return -1;
}

  void
pgn::Parser::skipBlanks(std::string::const_iterator &itr1,
    const std::string::const_iterator &end)
{
  while ((itr1 != end) && (isspace(*itr1)))
    itr1++;
}
