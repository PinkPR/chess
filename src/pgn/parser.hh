#ifndef parser_hh
# define parser_hh

# include <string>
# include <vector>
# include <iostream>
# include <fstream>
# include <sstream>
# include <stdexcept>
# include <cstdlib>
# include <vector>

# include "../game.hh"
# include "../sexy-move.hh"
# include "../move.hh"
# include "comment.hh"
# include "move.hh"
# include "move_list.hh"
# include "piece.hh"
# include "ply.hh"
# include "square.hh"
# include "tag.hh"
# include "exception.hh"

class Game;
class Move;

namespace pgn
{
  class MoveList;
  class Move;
  class Ply;
  class Comment;
  class Tag;

  class Parser
  {
    public:
      Parser():plyCount_(0),moveCount_(0) {}

      bool
      getGame(std::string::const_iterator &itr1,
              const std::string::const_iterator &itr2, Game &out);

      bool
      getMoveList(std::string::const_iterator &itr1,
                  const std::string::const_iterator &itr2, pgn::MoveList &out);
      bool
      getMove(std::string::const_iterator &itr1,
              const std::string::const_iterator &itr2, pgn::Move &out);

      bool
      getPly(std::string::const_iterator &itr1,
             const std::string::const_iterator &itr2, pgn::Ply &out);

      bool
      getTagList(std::string::const_iterator &itr1,
                 const std::string::const_iterator &itr2,
                 std::vector<pgn::Tag> &out);

      bool
      getTag(std::string::const_iterator &itr1,
             const std::string::const_iterator &itr2, pgn::Tag &out);

      bool
      getGameResult(std::string::const_iterator &itr1,
                    const std::string::const_iterator &itr2,
                    std::string &out);

      bool
      getComment(std::string::const_iterator &itr1,
                 const std::string::const_iterator &itr2, pgn::Comment &out);

      bool
      getMoveNumber(std::string::const_iterator &itr1,
                    const std::string::const_iterator &itr2,
                    std::string &out, int &dotsCount);

      int
      getGlyph(std::string::const_iterator &itr1);

      bool
      getVariation(std::string::const_iterator &itr1,
                   const std::string::const_iterator &itr2, pgn::MoveList &out);

      unsigned long plyCount() const { return plyCount_; }
      unsigned long moveCount() const { return moveCount_; }

    private:
      static void skipBlanks(std::string::const_iterator &itr1,
                             const std::string::const_iterator &end);

      unsigned long plyCount_;
      unsigned long moveCount_;
  };
}
#endif /* !parser_hh */
