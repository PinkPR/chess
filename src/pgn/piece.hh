#ifndef piece_hh
# define piece_hh

namespace pgn
{
  typedef enum { white, black, undef } Color;

  class Piece 
  {
    public:
      static const Piece Null();
      static const Piece Pawn();
      static const Piece Knight();
      static const Piece Bishop();
      static const Piece Rook();
      static const Piece Queen();
      static const Piece King();
      static const Piece WhitePawn();
      static const Piece WhiteKnight();
      static const Piece WhiteBishop();
      static const Piece WhiteRook();
      static const Piece WhiteQueen();
      static const Piece WhiteKing();
      static const Piece BlackPawn();
      static const Piece BlackKnight();
      static const Piece BlackBishop();
      static const Piece BlackRook();
      static const Piece BlackQueen();
      static const Piece BlackKing();

      Piece();
      Piece(const Piece &src);
      Piece(const char letter, Color c = undef);

      Piece& operator=(const Piece& src);
      bool operator==(const Piece& src) const;
      bool operator!=(const Piece& src) const; 

      char letter() const; // returns uppercase piece letter ('P' for pawn, '0' for null)
      Color color() const;

      friend std::ostream& operator<<(std::ostream& os, const pgn::Piece& src);

    private:
      char data_;
  };

  std::ostream& operator<<(std::ostream& os, const pgn::Piece& src);
};

#endif /* !piece_hh */
