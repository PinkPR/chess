#ifndef ply_hh
# define ply_hh

# include <fstream>
# include <string>
# include <vector>

# include "piece.hh"
# include "square.hh"
# include "comment.hh"

namespace pgn
{
  class MoveList;
  class Ply
  {
    public:
      Ply(const Ply& src);
      Ply(const std::string &ply_text);
      Ply();

      virtual ~Ply();

      Ply& operator = (const Ply& src);
      bool operator == (const Ply& src) const;
      bool operator != (const Ply& src) const;

      std::string str() const;
      bool isLongCastle() const;
      bool isShortCastle() const;
      bool isCapture() const;
      bool isCheck() const;
      bool isCheckMate() const;
      void bindComment(const Comment &comment);
      void unbindComment();
      void bindVariation(const MoveList &variation);
      void unbindVariations();
      void setGlyphValue(int val);
      int glyphValue() const;

      Square fromSquare() const; // if O-O or O-O-O returns a null square
      Square toSquare() const; // if O-O or O-O-O returns a null square
      void setFromSquare(const Square &s);
      void setToSquare(const Square &s);
      Piece piece() const;
      void setPiece(const Piece &p);
      bool promotion() const;
      Piece promoted() const;

      bool valid() const;

      friend std::ostream& operator<<(std::ostream& os, const pgn::Ply& src);
      friend std::istream& operator>>(std::istream& is, Ply& src);

    private:
      struct PlyData *hdata;
  };

  std::istream& operator>>(std::istream& is, Ply& src);
  std::ostream& operator<<(std::ostream& os, const Ply& src);
};

#endif /* !ply_hh */
