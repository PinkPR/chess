#ifndef position_hh
# define position_hh

# include <string> 
# include <exception>

# include "square.hh"
# include "ply.hh"
# include "piece.hh"

namespace pgn
{
  class Position 
  {
  public:
    Position();
    Position(const Position& src);
    Position(const std::string& fen);
    ~Position();

    Position& operator=(const Position& src);
    bool operator==(const Position& src) const;
    bool operator!=(const Position& src) const;

    /**
     * \brief Clear to an empty board
     */
    void clear();

    /**
     * \brief Reset to the initial chess position
     */
    void reset();

    std::string fen() const;

    Color sideToMove() const;
    void setSideToMove(Color c);

    int moveNumber() const;
    void setMoveNumber(int n);

    /**
     * \brief Counts half moves without capture and pawn moves.
     *
     * Usefull to apply the 50 moves draw rule.
     */
    int halfmoveClock() const;
    void setHalfmoveClock(int n);

    Piece pieceAt(char col, char row) const;
    void setPieceAt(const Piece &p, char col, char row);

    void update(Ply &ply);

    class iterator;
    friend class iterator;
    iterator begin() const;
    iterator end() const;

  private:
    struct PositionData *hdata;
  };

  class Position::iterator
  {
  public:
    typedef std::forward_iterator_tag iterator_category;
    typedef Square value_type;
    typedef Square* pointer;
    typedef Square& reference;
    typedef int difference_type;

    iterator();
    iterator(const Position &ml);
    iterator(const Position &ml, int);
    iterator(const iterator&);
    ~iterator();
    iterator& operator = (const iterator&);
    iterator& operator ++ ();
    iterator& operator ++ (int);
    pointer operator -> () const;
    reference operator * () const;
    bool operator == (const iterator&) const;
    bool operator != (const iterator&) const;

  private:
    struct iteratorData *hdata;
  };
};

#endif /* !position_hh */
