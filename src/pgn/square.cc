#include <sstream>
#include <iostream>

#include "square.hh"

pgn::Square::Square()
  :col_(0),row_(0),piece_(pgn::Piece::Null())
{
}

pgn::Square::Square(char col, char row, const pgn::Piece &piece)
  :col_(col),row_(row),piece_(piece)
{
}

pgn::Square::Square(const std::string &col_row, const pgn::Piece &piece)
  :col_(col_row.at(0)), row_(col_row.at(1)), piece_(piece)
{
}

pgn::Square::Square(const pgn::Square& src)
  :col_(src.col_),row_(src.row_),piece_(src.piece_)
{
}

pgn::Square::~Square()
{ 
}

bool
pgn::Square::valid() const
{
  return (col() != 0) && (row() != 0);
}

bool
pgn::Square::null() const
{
  return (col() == 0) && (row() == 0);
}

pgn::Square&
pgn::Square::operator=(const pgn::Square& src)
{
  if (&src == this)
    return (*this);

  col_ = src.col_;
  row_ = src.row_;
  piece_ = src.piece_;

  return(*this);
}

bool
pgn::Square::operator==(const pgn::Square& src) const
{
  return (col_ == src.col_) && (row_ == src.row_) && (piece_ == src.piece_);
}

char
pgn::Square::col() const
{
  return col_;
}

char
pgn::Square::row() const
{
  return row_;
}

std::string
pgn::Square::str() const
{
  if (null()) 
    return "";
  else
  {
    std::stringstream ss;
    ss << col() << row();
    return ss.str();
  }
}

pgn::Piece
pgn::Square::piece() const
{
  return piece_;
}

bool
pgn::Square::operator!=(const pgn::Square& src) const 
{
  return !(src == *this);
}

std::ostream&
pgn::operator<<(std::ostream& os, const pgn::Square& src) 
{
  if (src.col() != 0)
    os << src.col();

  if (src.row() != 0)
    os << src.row();

  return os;
}

std::istream&
pgn::operator>>(std::istream& is, pgn::Square& src) 
{
  std::string str;
  std::copy(std::istreambuf_iterator<char>(is),
            std::istreambuf_iterator<char>(), std::inserter(str, str.end()));
  src.col_ = str[0];
  src.row_ = str[1];
  return is;
}
