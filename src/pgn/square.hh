#ifndef square_hh
# define square_hh

# include <string>

# include "piece.hh"

namespace pgn 
{
  class Square 
  {
    public:
      Square();
      Square(char col, char row, const Piece &p = Piece::Null());
      Square(const std::string &col_row, const Piece &p = Piece::Null());
      Square(const Square& src);

      virtual ~Square();

      Square& operator = (const Square& src);
      bool operator == (const Square& src) const;
      bool operator != (const Square& src) const;

      char col() const; // returns 'a' for a4 square, 0 if undefined
      char row() const; // returns '4' for a4 square, 0 if undefined
      std::string str() const; // returns "a4" for a4 square, "" if undefined
      //			int colIndex() const; // returns 0 for a4 square
      //			int rowIndex() const; // returns 3 for a4 square
      Piece piece() const;

      bool valid() const; // 'a4' is a valid square. '4', 'a', '' are not.
      bool null() const; // '' is null, both row() and col() return zero.

      friend std::ostream& operator << ( std::ostream& os, const pgn::Square& src);
      friend std::istream& operator >> ( std::istream& is, Square& src);

    private:
      char col_;
      char row_;
      Piece piece_;
  };

  std::istream& operator >> ( std::istream& is, Square& src);
  std::ostream& operator << ( std::ostream& os, const Square& src);
};

#endif /* !square_hh */
