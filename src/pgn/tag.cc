#include <sstream>
#include <iostream>
#include <iterator>

#include "tag.hh"
#include "parser.hh"

pgn::Tag::Tag() 
{
}

pgn::Tag::Tag(const pgn::Tag& src) 
{
  name_ = src.name_;
  value_ = src.value_;
}

pgn::Tag::Tag(const std::string name, const std::string value)
{
  name_ = name;
  value_ = value;
}

pgn::Tag::~Tag() 
{
}

pgn::Tag& pgn::Tag::operator=(const pgn::Tag& src) 
{
  if (&src == this)
    return(*this);

  name_ = src.name_;
  value_ = src.value_;

  return *this;
}

bool
pgn::Tag::operator==(const pgn::Tag& other) const
{
  if (name_ != other.name_)
    return false;

  if (value_ != other.value_)
    return false;

  return true;
}

bool
pgn::Tag::operator!=(const pgn::Tag& src) const 
{ 
  return !(*this == src); 
}

bool
pgn::Tag::operator==(const std::string& name) const
{
  return name_ == name;
}

bool
pgn::Tag::operator!=(const std::string& other) const 
{ 
  return !(*this == other); 
}

std::string
pgn::Tag::name() const 
{ 
  return name_; 
}

std::string
pgn::Tag::value() const 
{ 
  return value_; 
}

std::ostream&
pgn::operator<<(std::ostream& os, const pgn::Tag& src) 
{
  os << "[" << src.name_ << " \"" << src.value_ << "\"]";
  return os;
}

std::istream&
pgn::operator>>(std::istream& is, pgn::Tag& src) 
{
  std::string str;
  std::copy(std::istreambuf_iterator<char>(is),
						std::istreambuf_iterator<char>(), std::inserter(str, str.end()));
  pgn::Parser parser;
  std::string::const_iterator itr1 = str.begin();
  std::string::const_iterator itr2 = str.end();
  parser.getTag(itr1, itr2, src);
  return is;
}
