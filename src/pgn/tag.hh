#ifndef tag_hh
# define tag_hh

# include <fstream>
# include <string>

namespace pgn
{
  class Tag 
  {
	public:
		Tag();
		Tag(const Tag& src);
		Tag(const std::string name, const std::string value);
		~Tag();

		Tag& operator=(const Tag& other);
		bool operator==(const Tag& other) const;
		bool operator!=(const Tag& other) const;
		bool operator==(const std::string& name) const;
		bool operator!=(const std::string& name) const;

		std::string name() const;
		std::string value() const;

		friend std::ostream& operator<<(std::ostream& os, const Tag& src);
		friend std::istream& operator>>(std::istream& is, Tag& src);

	private:
    std::string name_;
    std::string value_;
  };

  std::ostream& operator<<(std::ostream& os, const pgn::Tag& src);
  std::istream& operator>>(std::istream& is, Tag& src);
};

#endif /* !tag_hh */
