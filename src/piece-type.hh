#ifndef COMMON_PIECE_TYPE_HH
# define COMMON_PIECE_TYPE_HH

/*!
** \brief This enum represents the piece types present on the chessboard.
**
*/
enum PieceType
{
  KING = 0,
  QUEEN,
  ROOK,
  BISHOP,
  KNIGHT,
  PAWN,

  NONE
};

#endif // !COMMON_PIECE_TYPE_HH
