#include "sexy-listener.hh"

// TODO are those spaces or tabs around here ?

SexyListener::~SexyListener()
{
}

void
SexyListener::register_chessboard_adapter(const ChessboardAdapter&)
{
}

void
SexyListener::on_game_started()
{
  std::cout << "[game_started]\t" << std::endl;
}

void
SexyListener::on_game_finished()
{
  std::cout << "[game_finished]\t" << std::endl;
}

void
SexyListener::on_piece_moved(const PieceType& piece, const Position&,
                             const Position&)
{
  std::cout << "[move]\tPieceType: " << piece << std::endl;
}

void
SexyListener::on_piece_taken(const PieceType& piece, const Position& at)
{
  std::cout << "[piece_taken]\tpiece: " << piece <<" file: "
            << at.file_get() << " rank: " << at.rank_get() << std::endl;
}

void
SexyListener::on_piece_promoted(const PieceType& piece, const Position& at)
{
  std::cout << "[piece_promoted]\tpiece: " << piece << " file: "
            << at.file_get() << " rank: " << at.rank_get() << std::endl;
}

void
SexyListener::on_kingside_castling(const Color& color)
{
  std::cout << "[kingside castling]\tcolor: " << color << std::endl;
}

void
SexyListener::on_queenside_castling(const Color& color)
{
  std::cout << "[queenside castling]\tcolor: " << color << std::endl;
}

void
SexyListener::on_player_check(const Color& color)
{
  std::cout << "[check]\t: color: " << color << std::endl;
}

void
SexyListener::on_player_mat(const Color& color)
{
  std::cout << "[mat]\t: color: " << color << std::endl;
}

void
SexyListener::on_player_pat(const Color& color)
{
  std::cout << "[pat]\t: color: " << color << std::endl;
}

// TODO check with the ref as there is no exemple
void
SexyListener::on_player_timeout(const Color& color)
{
  std::cout << "[timeout]\t: color: " << color << std::endl;
}

void
SexyListener::on_player_disqualified(const Color& color)
{
  std::cout << "[disqualified]\t: color: " << color << std::endl;
}

void
SexyListener::on_draw()
{
  std::cout << "[draw]\t" << std::endl;
}


LISTENER_EXPORT("sexy_listener", new SexyListener());
