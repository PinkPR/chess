#include "sexy-move.hh"

SexyMove::SexyMove(Move move, int eaten, int transformation)
         : move_(move)
         , eaten_(eaten)
         , transformation_(transformation)
{
}

SexyMove::SexyMove(Move move, int eaten,
                   int transformation, Color color)
         : move_(move)
         , eaten_(eaten)
         , transformation_(transformation)
         , color_(color)
{
}

SexyMove::~SexyMove()
{
}

Move& SexyMove::get_move()
{
  return this->move_;
}

int& SexyMove::get_eaten()
{
  return this->eaten_;
}

int& SexyMove::get_transformation()
{
  return this->transformation_;
}

Color& SexyMove::get_color()
{
  return this->color_;
}
