#ifndef SEXY_MOVE_HH
# define SEXY_MOVE_HH

# define KING_CASTLING 1
# define QUEEN_CASTLING 2

# include "move.hh"
# include "piece-type.hh"
# include "color.hh"

class SexyMove : public Move
{
  public:
    SexyMove(Move Move, int eaten, int transformation);
    SexyMove(Move Move, int eaten, int transformation, Color color);
    ~SexyMove();

  public:
    Move& get_move();
    int& get_eaten();
    int& get_transformation();
    Color& get_color();

  private:
    Move move_;
    int eaten_;
    int transformation_;
    Color color_;
};

#endif /* SEXY_MOVE_HH */
