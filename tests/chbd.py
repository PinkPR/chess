#! /usr/bin/env python

class Chessboard:
    board = [
            ['r0', 'k0', 'b0', 'Q0', 'K0', 'b0', 'k0', 'r0'],
            ['p0', 'p0', 'p0', 'p0', 'p0', 'p0', 'p0', 'p0'],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            ['p1', 'p1', 'p1', 'p1', 'p1', 'p1', 'p1', 'p1'],
            ['r1', 'k1', 'b1', 'K1', 'Q1', 'b1', 'k1', 'r1'],
        ]

    def __init_(self):
        pass

    def display(self):
        print()
        print('    +---+---+---+---+---+---+---+---+')
        for l, line in enumerate(self.board):
            print(' \033[3;92m{}\033[0m  |'.format(8 - l), end='')
            for piece in line:
                if piece:
                    if piece[1] == '0':
                        color = '\033[31m'
                    else:
                        color = '\033[34m'

                    if piece[0] == 'Q' or piece[0] == 'K':
                        if piece[1] == '0':
                            color = '\033[30;41m'
                        else:
                            color = '\033[30;44m'

                    s = '*' if piece[0] == 'p' else piece[0] 
                    print(' {}{}\033[0m |'.format(color, s), end='')
                else:
                    print('   |', end='')
            print()
            if l < 7:
                print('    |---|---|---|---|---|---|---|---|')
        print('    +---+---+---+---+---+---+---+---+')
        print('\033[3;92m      a   b   c   d   e   f   g   h\033[0m\n')

chbd = Chessboard()
chbd.display()
